:slug: rules-new-standard/
:date: 2020-04-17
:subtitle: More requirements in Rules are firmly supported
:category: philosophy
:tags: security, cybersecurity, information, documentation, standard, ethical hacking
:image: cover.png
:alt: Photo by Bradley Feller on Unsplash
:description: Here we briefly outline Rules, Fluid Attacks's set of security requirements, along with the world-renowned standards that have served as a reference for us.
:keywords: Security, Cybersecurity, Information, Documentation, Standard, Rules, Ethical Hacking, Pentesting
:author: Felipe Ruiz
:writer: fruiz
:name: Felipe Ruiz
:about1: Technical writer
:about2: Behavioral scientist.
:source: https://unsplash.com/photos/lynE-l7F8sY

= We've Reached a New Standard

Earlier this month our `CTO`, Rafael Alvarez, informed us:
_"We've already finished the synthesis of the `GDPR` standard in Rules_."

What does that mean?

As Danilo Vásquez —Security Analyst at `Fluid Attacks`— explained to us,
different `GDPR` requirements, all of which were considered suitable,
became technical requirements for our company.
That is, they were incorporated into `Rules`.

Let's clear this up:

`Rules` was born there in *2009*
(inspired by link:https://www.commoncriteriaportal.org/[Common Criteria])
from the combination of knowledge and
"_experience in the field of computer security
with various international standards related to this topic_,"
according to Oscar Prado —Cybersecurity Analyst - Releaser at `Fluid Attacks`.

`Rules` can be defined —following Danilo—
as "_a compendium of requirements based on the existing security standards_."
Not all general security standards have been covered at this time.
Not all of our requirements are linked to
at least one of those standards we have.
But these are things we intend to achieve in the near future.

The technical requirements (we currently have [inner]#link:../../products/rules/list/[*189* in *20* categories]#),
apart from those that may be [inner]#link:../../products/rules/list/out-of-scope/["out-of-scope,"]#
are linked to what is verifiable through a technical process in a system.
They are those that can be evaluated with [inner]#link:../fuzzing-forallsecure/[`DAST`]#, [inner]#link:../sast-sca-kiuwan/[`SAST`, or `SCA`]#,
techniques that we have already mentioned.

The out-of-scope security requirements, which can focus, for example,
on security training and documentation,
in contrast, can only be verified by audits.
And although they are not technical,
as Oscar says, "_they play a key role in a security test.
For example, the lack of training of an employee
can allow a [inner]#link:../phishing/[phishing]# attack to take place_."
Even some of them may later become technical requirements.

The requirements that we have in `Rules`
have kept a consistent and simple language, easy to understand.
They allow to delimit a pentesting or an ethical hacking process.
The delimitation will depend on what each client wants to test
and on the concepts of risk and vulnerability that he handles.
The requirements that are chosen
determine the stringency and extent of pentesting.

Hackers have in `Rules`,
as Paula Vélez, Security Architect at `Fluid Attacks` suggests,
"_a performance guide, because if a requirement is not being satisfied,
that means there is a vulnerability to be reported_."
Hacking is made easier; time and effort are saved
if the requirements are found in a single set or single source.

Likewise, clients save time thanks to `Rules`,
as they don't have to resort to a multiplicity of standards
that fit their security needs.
In `Rules`, we synthesize, and we handle homogeneity of expression
no matter the diversity of origins of the base standards.

`Rules`, as a whole, ends up presenting good practices
not only to developers and configurators of products and systems
but also to other members of the organizations
that can influence their security.
The requirements serve as a guide from the beginning or in full flow.
They serve to prevent the manifestation
or recognize the presence of security gaps
—all this independently of the technology that is being used.

So, we know that many of our requirements
are already associated with one or more reputable standards.
But what are these standards that currently serve us as references?
Let's finish this blog post by mentioning them.

== Standards

.link:https://unsplash.com/photos/1wDyL2_NmE4[Photo by Mathieu Perrier] on Unsplash
image::standards.png[standards]

. link:https://www.hhs.gov/hipaa/index.html[`HIPAA`]
+
Health Insurance Portability and Accountability Act,
link:https://en.wikipedia.org/wiki/Health_Insurance_Portability_and_Accountability_Act[enacted by the United States Congress in 1996].
As we found in link:https://www.hipaaguide.net/hipaa-for-dummies/[a guide],
this legislation was created, among other objectives,
for the modernization of the flow of healthcare information.
Besides, it was for the stipulation of how
the `PII` ([inner]#link:../pii-leakage-whitehat/[Personally Identifiable Information]#),
within the healthcare context,
should be protected by industries to avoid theft and fraud.
The `HIPAA` comprises five essential rules, each with different requirements.
These rules are: *1)* `HIPAA` Privacy Rule, *2)* `HIPAA` Security Rule,
*3)* Breach Notification Rule, *4)* Omnibus Rule, and *5)* Enforcement Rule.

. link:https://owasp.org/www-project-application-security-verification-standard/[`ASVS`]
+
The Application Security Verification Standard
is a project from the Open Web Application Security Project (link:https://en.wikipedia.org/wiki/OWASP[`OWASP`]),
an online community for the production of freely-available material
in the field of web app security.
`ASVS`, as we can see on its link:https://owasp.org/www-project-application-security-verification-standard/[website],
"_provides a basis for testing web application technical security controls_."
It also provides developers with requirements oriented to secure development,
protecting from vulnerabilities in web apps.
These requirements can also be used by anyone
who wants to build and test secure apps.

. link:https://cwe.mitre.org/[`CWE`]
+
Common Weakness Enumeration, as we found on its link:https://cwe.mitre.org/[website],
"_is a community-developed list
of common software and hardware security weaknesses_."
The `CWE` list serves as a support, with a common language,
for the identification of vulnerabilities.
Such identification is essential for their subsequent reduction or repair,
and also for prevention.
The `CWE` has an orientation towards the communities
of development and security professionals.
It seeks to help ensure that weaknesses in software and hardware
are remediated before the delivery of products,
or that they are prevented to avoid putting organizations at risk.

. link:https://www.nist.gov/about-nist[`NIST`]
+
The National Institute of Standards and Technology,
from the United States, has been active for more than a century.
`NIST` provides different measurements, standards, and technologies
to support a variety of scientifically based,
human-made products and services, regardless of their size and complexity.
Inside `NIST` is the Information Technology Laboratory (link:https://www.nist.gov/itl/about-itl[`ITL`]),
and linked to it is the National Vulnerability Database (link:https://nvd.nist.gov/general[`NVD`]).
The `NVD`, created in *2000*, "_is the `U.S.` government repository
of standards based vulnerability management data
represented using the Security Content Automation Protocol (`SCAP`).
This data enables automation of vulnerability management,
security measurement, and compliance_.”
At `Fluid Attacks`, we are mainly interested
in the link:https://nvd.nist.gov/800-53/Rev4[`NIST` Special Publication 800-53 (Revision 4)].
It is a database associated with security and privacy controls
for federal information systems and organizations.

. link:https://www.bsimm.com/[`BSIMM`]
+
Building Security In Maturity Model,
as expressed on its link:https://www.bsimm.com/[website],
"is a study of existing software security initiatives [`SSIs`]_."
It quantifies the security practices of many organizations
and describes a common ground and variations.
It is a data-driven model resulting from the analysis
of those initiatives or "_application/product security programs_"
to face the challenge of securing the software.
`BSIMM’s` descriptive model has evolved
in line with advances in the field of security.
It has also grown by collecting and analyzing new data
from new companies and those with maturing programs.
At `Fluid Attacks`, we use its ninth iteration.

. link:https://gdpr-info.eu/[`GDPR`]
+
General Data Protection Regulation is a set of rules
within the European Union (`EU`) and the European Economic Area (`EEA`)
link:https://en.wikipedia.org/wiki/General_Data_Protection_Regulation[about data protection and privacy].
These rules also address the collection, storage, and transfer of data
from European subjects outside those areas.
The `GDPR` enables the unification of international regulations
for organizations processing personal data of European citizens.
It also aims to give individuals more control
over their personal information and its treatment.
Companies are required to apply data protection principles
(using those strictly necessary)
in processing activities and business practices from the initial stages.

Do you want more information about it? [inner]#link:../../contact-us/[Contact us!]#
