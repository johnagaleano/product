:clientlogo: logo-berkeley
:alt: Logo Berkeley Payment
:client: yes
:filter: banking

= Berkeley Payment

*Berkeley Payment* is a Canadian company that manages over a billion dollars for
organizations, governments, and corporations.
