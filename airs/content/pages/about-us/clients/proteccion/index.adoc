:clientlogo: logo-proteccion
:alt: Logo Proteccion
:client: yes
:filter: banking

= Protección

*Protección* is a Colombian pension and severance fund administrator that manages
and invests millions of workers’ contributions.
