:slug: about-us/partners/greensqa/
:description: Our partners allow us to complete our portfolio and offer better security testing services. Get to know them and become one of them.
:keywords: Fluid Attacks, Partners, Services, Security Testing, Software Development, Pentesting, Ethical Hacking
:partnerlogo: logo-greensqa
:alt: Logo GreenSQA
:partner: yes

= GreenSQA

GreenSQA is a company with `15` years of experience in software testing,
such as performance tests and tests on different mobile devices.
It tests automation throughout the product development life-cycle.
