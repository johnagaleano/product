:slug: about-us/people/aroldan/
:category: about-us
:peoplepage: yes
:description: Fluid Attacks is a company focused on ethical hacking and pentesting in applications with over 18 year of experience providing our services to the Colombian market. The purpose of this page is to present a small overview about the experience, education and achievements of Andres Roldan.
:keywords: Fluid Attacks, Team, People, Members, Andres, Roldan.

= Andres Roldan

[role="img-ppl"]
image::aroldan.png[Andres Roldan]

Cyber Security Specialist from the University of Maryland. `Fluid Attacks`
partner and Senior Security Architect with *20* years of experience in
security-related tasks, responsible for leading advanced ethical hackings
and red team engagements.

He has held the `CEH` certification several times and currently holds
the `OSCP` certification.
He has been an active official Debian Developer for *14* years and had
frequently attended cutting-edge security conferences and training in
countries like the `USA`, `Israel`, `Argentina`, and `Colombia`.
