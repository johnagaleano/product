:slug: products/rules/findings/038/
:description: The purpose of this page is to present information about the set of findings reported by Fluid Attacks. In this case, the finding presents information about vulnerabilities related with business information leaks, recommendations to avoid them and related security requirements.
:keywords: Business, Information, Data, Leak, Exposed, Confidential
:findings: yes
:type: security

= F038. Business information leak

== Description

It is possible to obtain business information, such as:

* Username list

* Strategic information

* Employees information

* Clients information

* Providers information

== Rules

. [[r1]] [inner]#link:/products/rules/list/123/[R123. Restrict reading of emails]#

. [[r2]] [inner]#link:/products/rules/list/176/[R176. Restrict system objects]#

. [[r3]] [inner]#link:/products/rules/list/177/[R177. Avoid caching and temporary files]#

. [[r4]] [inner]#link:/products/rules/list/261/[R261. Avoid exposing sensitive information]#

. [[r5]] [inner]#link:/products/rules/list/300/[R300. Mask sensitive data]#
