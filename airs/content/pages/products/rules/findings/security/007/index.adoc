:slug: products/rules/findings/007/
:description: The purpose of this page is to present information about the set of findings reported by Fluid Attacks. In this case, the finding presents information about cross-site request forgery attacks, recommendations to avoid them and related security requirements.
:keywords: Cross-site, Request, Forgery, Attack, Console, Injection
:findings: yes
:type: security

= F007. Cross-site request forgery

== Description

The application's configuration allows an attacker to trick authenticated users
into executing actions without their consent.

== Rules

. [[r1]] [inner]#link:/products/rules/list/029/[R029. Cookies with security attributes]#

. [[r2]] [inner]#link:/products/rules/list/174/[R174. Transactions without distinguishable pattern]#
