:slug: products/rules/findings/005/
:description: The purpose of this page is to present information about the set of findings reported by Fluid Attacks. In this case, the finding presents information about privilege escalation attacks, recommendations to avoid them and related security requirements.
:keywords: Privilege, Escalation, Role, Administrative Access, Permissions, Configuration
:findings: yes
:type: security

= F005. Privilege escalation

== Description

Due to an insecure role configuration,
it is possible to obtain administrative access or privileges using a standard
user account.

== Rules

. [[r1]] [inner]#link:/products/rules/list/035/[R035. Manage privilege modifications]#
