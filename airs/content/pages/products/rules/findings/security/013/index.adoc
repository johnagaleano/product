:slug: products/rules/findings/013/
:description: The purpose of this page is to present information about the set of findings reported by Fluid Attacks. In this case, the finding presents information about vulnerabilities arising from insecure object references, recommendations to avoid them and related security requirements.
:keywords: Reference, Insecure, Object, Authorization, Bypass, Data
:findings: yes
:type: security

= F013. Insecure object reference

== Description

The system's authorization mechanism does not prevent one user from accessing
another user's data by modifying the key value that identifies it.

== Rules

. [[r1]] [inner]#link:/products/rules/list/176/[R176. Restrict system objects]#
