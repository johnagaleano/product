:slug: products/rules/findings/085/
:description: The purpose of this page is to present information about the set of findings reported by Fluid Attacks. In this case, the finding presents information about vulnerabilities arising from improperly using the local storage, recommendations to avoid them and related security requirements.
:keywords: Sensitive, Information, Data, Local, Session, Storage
:findings: yes
:type: security

= F085. Sensitive data stored in client-side storage

== Description

The application stores sensitive information in the client-side storage
(*localStorage* or *sessionStorage*).
This exposes the information to unauthorized read and write operations.

== Rules

. [[r1]] [inner]#link:/products/rules/list/177/[R177. Avoid caching and temporary files]#

. [[r2]] [inner]#link:/products/rules/list/329/[R329. Keep client-side storage without sensitive data]#
