:slug: products/rules/findings/039/
:description: The purpose of this page is to present information about the set of findings reported by Fluid Attacks. In this case, the finding presents information about vulnerabilities arising from not properly controlling access to web services, recommendations to avoid them and related security requirements.
:keywords: Web, Services, Authorization, Bypass, Control, Mechanism
:findings: yes
:type: security

= F039. Improper authorization control for web services

== Description

The system's web services do not have an authorization control mechanism or the
one in place can be bypassed.

== Rules

. [[r1]] [inner]#link:/products/rules/list/096/[R096. Set user required privileges]#

. [[r2]] [inner]#link:/products/rules/list/176/[R176. Restrict system objects]#

. [[r3]] [inner]#link:/products/rules/list/265/[R265. Restrict access to critical processes]#

. [[r4]] [inner]#link:/products/rules/list/320/[R320. Avoid client-side control enforcement]#
