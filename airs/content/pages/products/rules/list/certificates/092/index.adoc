:slug: products/rules/list/092/
:category: certificates
:description: This requirement establishes that certificates, to use within the organization for external apps, must be signed by valid external certification bodies.
:keywords: Certificate, Certification Bodies, Applications, Signature, Requirement, CAPEC, CWE, Security, Rules, Ethical Hacking, Pentesting
:rules: yes

= R092. Use externally signed certificates

== Requirement

The organization must use certificates
signed by valid external certification authorities
when these are for external applications.

== Findings

* [inner]#link:/products/rules/findings/049/[F049. Insecure digital certificates]#

== References

. [[r1]] link:http://capec.mitre.org/data/definitions/94.html[CAPEC-94: Man in the Middle Attack].
This type of attack targets the communication between two components
(typically client and server).
The attacker places themself in the communication channel between the two
components.
Whenever one component attempts to communicate with the other
(data flow, authentication challenges, etc.),
the data first goes to the attacker,
who has the opportunity to observe or alter it,
and it is then passed on to the other component as if it was never observed.

. [[r2]] link:https://cwe.mitre.org/data/definitions/300.html[CWE-300: Channel Accessible by Non-Endpoint].
The product does not adequately verify the identity of actors at both ends of a
communication channel,
or does not adequately ensure the integrity of the channel,
in a way that allows the channel to be accessed or influenced by an actor that
is not an endpoint.
