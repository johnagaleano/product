:slug: products/rules/list/173/
:category: source
:description: This requirement establishes the importance of validating the application inputs and discarding harmful information to avoid frequent injection attacks.
:keywords: Inputs, Application, Validation, Discard, ASVS, CAPEC, CWE, OWASP, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R173. Discard unsafe inputs

== Requirement

The system must discard all potentially harmful information
received via data inputs.

== Description

Technological devices and, in particular, applications
must be able to notice if the received information
does not correspond to its operational purposes,
in order to treat it properly
(for example, rejecting and/or generating timely alerts)
and ensure that it does not impact the operation negatively.
Typical examples of this are: *SQL* queries, *JavaScript* code,
*OS* commands or *LDAP* queries
in fields and application parameters;
text with undesired special characters
and their possible combinations in fields and parameters;
files manipulated in structure and extension
to be loaded in an application or technology artifact;
and in general any type of information
that does not correspond to the requested format.
A large amount of the incoming traffic
(which does not correspond to operational purposes)
of a technological artifact must also be considered,
and controls and proper treatment must be applied.

== Implementation

. Perform whitelisting instead of blacklisting:
By using this principle, the control implementation
will be configured to reject all inputs
that are not explicitly approved,
thus discarding all possible scenarios
and forcing enumeration of strictly allowed inputs
(e.g., allow only *a-z*, *A-Z* and *0-9*).

. Use variable highlighting: If supported,
language and compiler can detect foreign variables
that are not validated by a filter.
To this end, you may use techniques to highlight those variables
and compilers, or pre-processors to detect that they are not validated.

. Not only user validation but also input validation:
Any input from outside the application
must be considered malicious.
In other words, the user can enter incorrect information,
as well as potentially harmful inputs,
that may also compromise external systems,
and therefore this information must be validated.

== Attacks

. Inject malicious software or code.
. Denial of service caused by register overload.
. Cross-Site Scripting (*XSS*).
. *OS* command injection.
. *SQL* injection.
. *LDAP* injection.
. Redirect to untrusted pages.

== Attributes

* Layer: Application layer
* Asset: Source code
* Scope: Maturity
* Phase: Building
* Type of control: Recommendation

== Findings

* [inner]#link:/products/rules/findings/001/[F001. SQL injection]#

* [inner]#link:/products/rules/findings/004/[F004. Remote command execution]#

* [inner]#link:/products/rules/findings/008/[F008. Reflected cross-site scripting (XSS)]#

* [inner]#link:/products/rules/findings/010/[F010. Stored cross-site scripting (XSS)]#

* [inner]#link:/products/rules/findings/021/[F021. XPath Injection]#

* [inner]#link:/products/rules/findings/023/[F023. Uncontrolled external site redirect]#

* [inner]#link:/products/rules/findings/032/[F032. Spoofing]#

* [inner]#link:/products/rules/findings/045/[F045. HTML code injection]#

* [inner]#link:/products/rules/findings/063/[F063. Lack of data validation]#

* [inner]#link:/products/rules/findings/067/[F067. Improper resource allocation]#

* [inner]#link:/products/rules/findings/083/[F083. XML Injection (XXE)]#

* [inner]#link:/products/rules/findings/090/[F090. Code injection (CSV)]#

* [inner]#link:/products/rules/findings/091/[F091. Log injection]#

* [inner]#link:/products/rules/findings/093/[F093. Hidden fields manipulation]#

* [inner]#link:/products/rules/findings/096/[F096. Insecure deserialization]#

* [inner]#link:/products/rules/findings/097/[F097. Reverse tabnabbing]#

* [inner]#link:/products/rules/findings/100/[F100. Server-side request forgery (SSRF)]#

* [inner]#link:/products/rules/findings/103/[F103. Insufficient data authenticity validation]#

* [inner]#link:/products/rules/findings/105/[F105. Apache Lucene query injection]#

* [inner]#link:/products/rules/findings/106/[F106. NoSQL injection]#

* [inner]#link:/products/rules/findings/107/[F107. LDAP injection]#

* [inner]#link:/products/rules/findings/110/[F110. HTTP request smuggling]#

== References

. [[r1]] link:http://capec.mitre.org/data/definitions/3.html[CAPEC-3: Using Leading 'Ghost' Character Sequences to Bypass Input Filters].
An adversary can intentionally introduce leading "ghost" characters
(extra characters that don't affect the validity of the request at the *API*
layer)
that enable the input to pass the filters and therefore process the adversary's
input.

. [[r2]] link:http://capec.mitre.org/data/definitions/4.html[CAPEC-4: Using Alternative IP Address Encodings].
Networked applications may expect network location information in a specific
format,
such as fully qualified domains names (**FQDN**s), *URL*, *IP* address,
or *IP* Address ranges.
If the location information is not validated against a variety of different
possible encodings and formats,
the adversary can use an alternate format to bypass application access control.

. [[r3]] link:http://capec.mitre.org/data/definitions/6.html[CAPEC-6: Argument Injection].
An attacker changes the behavior or state of a targeted application through
injecting data or command syntax through the targets use of non-validated and
non-filtered arguments of exposed services or methods.

. [[r4]] link:http://capec.mitre.org/data/definitions/7.html[CAPEC-7: Blind SQL Injection].
Blind *SQL* Injection results from an insufficient mitigation for *SQL*
Injection.
Blind *SQL* Injection is a form of *SQL* Injection that overcomes the lack of
error messages.

. [[r5]] link:http://capec.mitre.org/data/definitions/15.html[CAPEC-15: Command Delimiters].
The system that uses a filter or denylist input validation, as opposed to
allowlist validation is vulnerable to an attacker who predicts delimiters
(or combinations of delimiters) not present in the filter or denylist.

. [[r6]] link:http://capec.mitre.org/data/definitions/18.html[CAPEC-18: XSS Targeting Non-Script Elements].
This attack is a form of Cross-Site Scripting (*XSS*) where malicious scripts
are embedded in elements that are not expected to host scripts such as image
tags (**<img>**), comments in *XML* documents (**<!-CDATA->**), etc.
These tags may not be subject to the same input validation, output validation,
and other content filtering and checking routines.

. [[r7]] link:http://capec.mitre.org/data/definitions/19.html[CAPEC-19: Embedding Scripts within Scripts].
An attack of this type exploits a programs' vulnerabilities that are brought on
by allowing remote hosts to execute scripts.
The adversary leverages this capability to execute their own script by
embedding it within other scripts that the target software is likely to
execute.
The adversary must have the ability to inject their script into a script that
is likely to be executed.

. [[r8]] link:http://capec.mitre.org/data/definitions/22.html[CAPEC-22: Exploiting Trust in Client].
An attack of this type exploits vulnerabilities in client/server communication
channel authentication and data integrity.
It leverages the implicit trust a server places in the client,
or more importantly, that which the server believes is the client.

. [[r9]] link:http://capec.mitre.org/data/definitions/32.html[CAPEC-32: XSS Through HTTP Query Strings].
An adversary embeds malicious script code in the parameters of an *HTTP* query
string and convinces a victim to submit the *HTTP* request that contains the
query string to a vulnerable web application.
The web application then procedes to use the values parameters without properly
validation them first and generates the HTML code that will be executed by the
victim's browser.

. [[r10]] link:http://capec.mitre.org/data/definitions/34.html[CAPEC-34: HTTP Response Splitting].
This attack uses a maliciously-crafted *HTTP* request in order to cause a
vulnerable web server to respond with an *HTTP* response stream that will be
interpreted by the client as two separate responses instead of one.
This is possible when user-controlled input is used unvalidated as part of the
response headers.

. [[r11]] link:http://capec.mitre.org/data/definitions/41.html[CAPEC-41: Using Meta-characters in E-mail Headers to Inject Malicious Payloads].
This type of attack involves an attacker leveraging meta-characters in email
headers to inject improper behavior into email programs.

. [[r12]] link:http://capec.mitre.org/data/definitions/48.html[CAPEC-48: Passing Local Filenames to Functions That Expect a URL].
This attack relies on client side code to access local files and resources
instead of **URL**s.
When the client browser is expecting a *URL* string,
but instead receives a request for a local file,
that execution is likely to occur in the browser process space with the
browser's authority to local files.

. [[r13]] link:http://capec.mitre.org/data/definitions/130.html[CAPEC-130: Excessive Allocation].
An adversary causes the target to allocate excessive resources to servicing the
attackers' request,
thereby reducing the resources available for legitimate services and degrading
or denying services.
Usually, this attack focuses on memory allocation,
but any finite resource on the target could be the attacked, including
bandwidth, processing cycles, or other resources.

. [[r14]] link:http://capec.mitre.org/data/definitions/137.html[CAPEC-137: Parameter Injection].
An adversary manipulates the content of request parameters for the purpose of
undermining the security of the target.

. [[r15]] link:http://capec.mitre.org/data/definitions/153.html[CAPEC-153: Input Data Manipulation].
An attacker exploits a weakness in input validation by controlling the format,
structure, and composition of data to an input-processing interface.
By supplying input of a non-standard or unexpected form an attacker can
adversely impact the security of the target.

. [[r16]] link:http://capec.mitre.org/data/definitions/175.html[CAPEC-175: Code Inclusion].
An adversary exploits a weakness on the target to force arbitrary code to be
retrieved locally or from a remote location and executed.

. [[r17]] link:http://capec.mitre.org/data/definitions/240.html[CAPEC-240: Resource Injection].
An adversary exploits weaknesses in input validation by manipulating resource
identifiers enabling the unintended modification or specification of a
resource.

. [[r18]] link:http://capec.mitre.org/data/definitions/242.html[CAPEC-242: Code Injection].
An adversary exploits a weakness in input validation on the target to inject
new code into that which is currently executing.

. [[r19]] link:http://capec.mitre.org/data/definitions/248.html[CAPEC-248: Command Injection].
An adversary looking to execute a command of their choosing,
injects new items into an existing command thus modifying interpretation away
from what was intended.

. [[r20]] link:https://www.cisecurity.org/controls/[CIS Controls. 18.2 Ensure That Explicit Error Checking Is Performed
for All In-House Developed Software].
For in-house developed software, ensure that explicit error checking is
performed and documented for all input,
including for size, data type, and acceptable ranges or formats.

. [[r21]] link:https://cwe.mitre.org/data/definitions/20.html[CWE-20: Improper Input Validation].
The product does not validate or incorrectly validates input that can affect
the control flow or data flow of a program.

. [[r22]] link:https://cwe.mitre.org/data/definitions/74.html[CWE-74: Injection].
The software constructs all or part of a command, data structure, or record
using externally-influenced input from an upstream component,
but it does not neutralize or incorrectly neutralizes special elements.

. [[r23]] link:https://cwe.mitre.org/data/definitions/78.html[CWE-78: OS Command Injection].
The software constructs all or part of an OS command using
externally-influenced input from an upstream component,
but it does not neutralize or incorrectly neutralizes special elements that
could modify the intended OS command.

. [[r24]] link:https://cwe.mitre.org/data/definitions/79.html[​CWE-79: Cross-site Scripting].
The software does not neutralize or incorrectly neutralizes user-controllable
input before it is placed in output that is used as a web page that is served
 to other users.

. [[r25]] link:https://cwe.mitre.org/data/definitions/80.html[CWE-80: Basic XSS].
The software receives input from an upstream component,
but it does not neutralize or incorrectly neutralizes special characters such
as "<", ">", and "&" that could be interpreted as web-scripting elements.

. [[r26]] link:https://cwe.mitre.org/data/definitions/89.html[CWE-89: SQL Injection].
The software constructs all or part of an SQL command using
externally-influenced input from an upstream component,
but it does not neutralize or incorrectly neutralizes special elements that
could modify the intended SQL command.

. [[r27]] link:https://cwe.mitre.org/data/definitions/94.html[CWE-94: Code Injection].
The software constructs all or part of a code segment using
externally-influenced input from an upstream component,
but it does not neutralize or incorrectly neutralizes special elements that
could modify the syntax or behavior of the intended code segment.

. [[r28]] link:https://cwe.mitre.org/data/definitions/116.html[CWE-116: Improper Encoding or Escaping of Output].
The software prepares a structured message for communication with another
component,
but encoding or escaping of the data is either missing or done incorrectly.
As a result, the intended structure of the message is not preserved.

. [[r29]] link:https://cwe.mitre.org/data/definitions/138.html[CWE-138: Improper Neutralization of Special Elements].
The software receives input from an upstream component,
but it does not neutralize or incorrectly neutralizes special elements that
could be interpreted as control elements or syntactic markers when they are
sent to a downstream component.

. [[r30]] link:https://cwe.mitre.org/data/definitions/147.html[CWE-147: Improper Neutralization of Input Terminators].
The software receives input from an upstream component,
but it does not neutralize or incorrectly neutralizes special elements that
could be interpreted as input terminators when they are sent to a downstream
component.

. [[r31]] link:https://cwe.mitre.org/data/definitions/159.html[CWE-159: Improper Handling of Invalid Use of Special Elements].
The product does not properly filter, remove, quote, or otherwise manage the
invalid use of special elements in user-controlled input,
which could cause adverse effect on its behavior and integrity.

. [[r32]] link:https://cwe.mitre.org/data/definitions/602.html[CWE-602: Client-Side Enforcement of Server-Side Security].
The software is composed of a server that relies on the client to implement a
mechanism that is intended to protect the server.

. [[r33]] link:https://cwe.mitre.org/data/definitions/643.html[CWE-643: XPath Injection].
The software uses external input to dynamically construct an *XPath* expression
used to retrieve data from an *XML* database,
but it does not neutralize or incorrectly neutralizes that input.

. [[r34]] link:https://cwe.mitre.org/data/definitions/943.html[CWE-943: Improper Neutralization of Special Elements in Data Query Logic].
The application generates a query intended to access or manipulate data in a
data store such as a database,
but it does not neutralize or incorrectly neutralizes special elements that can
modify the intended logic of the query.

. [[r35]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A1-Injection[OWASP Top 10 A1:2017-Injection].
Injection flaws, such as **SQL**, **NoSQL**, **OS**, and *LDAP* injection,
occur when untrusted data is sent to an interpreter as part of a command or
query.
The attacker's hostile data can trick the interpreter into executing unintended
commands or accessing data without proper authorization.

. [[r36]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A4-XML_External_Entities_(XXE)[OWASP Top 10 A4:2017-XML External Entities (XXE)].
Many older or poorly configured *XML* processors evaluate external entity
references within *XML* documents.
External entities can be used to disclose internal files using the file *URI*
handler, internal file shares, internal port scanning, remote code execution,
and denial of service attacks.

. [[r37]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A7-Cross-Site_Scripting_(XSS)[OWASP Top 10 A7:2017-Cross-Site Scripting (XSS)].
*XSS* flaws occur whenever an application includes untrusted data in a new web
page without proper validation or escaping,
or updates an existing web page with user-supplied data using a browser *API*
that can create *HTML* or **JavaScript**.
*XSS* allows attackers to execute scripts in the victim's browser which can
hijack user sessions, deface web sites, or redirect the user to malicious
sites.

. [[r38]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A8-Insecure_Deserialization[OWASP Top 10 A8:2017-Insecure Deserialization].
Insecure deserialization often leads to remote code execution.
Even if deserialization flaws do not result in remote code execution,
they can be used to perform attacks,
including replay attacks, injection attacks, and privilege escalation attacks.

. [[r39]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V1.5 Input and Output Architectural Requirements.(1.5.3)]
Verify that input validation is enforced on a trusted service layer.

. [[r40]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V1.5 Input and Output Architectural Requirements.(1.5.4)]
Verify that output encoding occurs close to or by the interpreter for which it
is intended.

. [[r41]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.1 Input Validation Requirements.(5.1.1)]
Verify that the application has defenses against HTTP parameter pollution
attacks,
particularly if the application framework makes no distinction about the source
of request parameters (*GET*, *POST*, cookies, headers,
or environment variables).

. [[r42]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.1 Input Validation Requirements.(5.1.3)]
Verify that all input (*HTML* form fields, *REST* requests, *URL* parameters,
*HTTP* headers, cookies, batch files, *RSS* feeds, etc) is validated using
positive validation (*whitelisting*).

. [[r43]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.1 Input Validation Requirements.(5.1.4)]
Verify that structured data is strongly typed and validated against a defined
schema including allowed characters, length and pattern
(e.g., credit card numbers or telephone, or validating that two related fields
are reasonable, such as checking that suburb and zip/postcode match).

. [[r44]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.2 Sanitization and Sandboxing Requirements.(5.2.1)]
Verify that all untrusted *HTML* input from *WYSIWYG* editors or similar is
properly sanitized with an *HTML* sanitizer library or framework feature.

. [[r45]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.2 Sanitization and Sandboxing Requirements.(5.2.2)]
Verify that unstructured data is sanitized to enforce safety measures such as
allowed characters and length.

. [[r46]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.2 Sanitization and Sandboxing Requirements.(5.2.3)]
Verify that the application sanitizes user input before passing to mail systems
to protect against *SMTP* or *IMAP* injection.

. [[r47]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.2 Sanitization and Sandboxing Requirements.(5.2.4)]
Verify that the application avoids the use of **eval()** or other dynamic code
execution features.
Where there is no alternative,
any user input being included must be sanitized or sandboxed before being
executed.

. [[r48]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.2 Sanitization and Sandboxing Requirements.(5.2.5)]
Verify that the application protects against template injection attacks by
ensuring that any user input being included is sanitized or sandboxed.

. [[r49]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.2 Sanitization and Sandboxing Requirements.(5.2.6)]
Verify that the application protects against *SSRF* attacks,
by validating or sanitizing untrusted data or *HTTP* file metadata,
such as filenames and URL input fields,
or using whitelisting of protocols, domains, paths and ports.

. [[r50]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.2 Sanitization and Sandboxing Requirements.(5.2.7)]
Verify that the application sanitizes, disables, or sandboxes user-supplied
*SVG* scriptable content,
especially as they relate to *XSS* resulting from inline scripts, and
*foreignObject*.

. [[r51]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.2 Sanitization and Sandboxing Requirements.(5.2.8)]
Verify that the application sanitizes, disables, or sandboxes user-supplied
scriptable or expression template language content,
such as Markdown, *CSS* or *XSL* stylesheets, *BBCode*, or similar.

. [[r52]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.3 Output encoding and Injection Prevention Requirements.(5.3.4)]
Verify that data selection or database queries
(e.g., *SQL*, *HQL*, *ORM*, *NoSQL*) use parameterized queries, *ORMs*,
entity frameworks, or are otherwise protected from database injection attacks.

. [[r53]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.3 Output encoding and Injection Prevention Requirements.(5.3.5)]
Verify that where parameterized or safer mechanisms are not present,
context-specific output encoding is used to protect against injection attacks,
such as the use of *SQL* escaping to protect against *SQL* injection.

. [[r54]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.3 Output encoding and Injection Prevention Requirements.(5.3.7)]
Verify that the application protects against *LDAP* Injection vulnerabilities,
or that specific security controls to prevent *LDAP* Injection have been
implemented.

. [[r55]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.3 Output encoding and Injection Prevention Requirements.(5.3.8)]
Verify that the application protects against *OS* command injection and that
operating system calls use parameterized *OS* queries or use contextual command
line output encoding.

. [[r56]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.3 Output encoding and Injection Prevention Requirements.(5.3.9)]
Verify that the application protects against Local File Inclusion (*LFI*) or
Remote File Inclusion (*RFI*) attacks.

. [[r57]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.3 Output encoding and Injection Prevention Requirements.(5.3.10)]
Verify that the application protects against *XPath* injection or *XML*
injection attacks.

. [[r58]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.4 Memory, String, and Unmanaged Code Requirements.(5.4.2)]
Verify that format strings do not take potentially hostile input,
and are constant.

. [[r59]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.4 Memory, String, and Unmanaged Code Requirements.(5.4.3)]
Verify that sign, range, and input validation techniques are used to prevent
integer overflows.

. [[r60]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V5.5 Deserialization Prevention Requirements.(5.5.2)]
Verify that the application correctly restricts *XML* parsers to only use the
most restrictive configuration possible and to ensure that unsafe features such
as resolving external entities are disabled to prevent *XXE*.

. [[r61]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V7.3 Log Protection Requirements.(7.3.1)]
Verify that the application appropriately encodes user-supplied data to prevent
log injection.

. [[r62]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V12.1 File Upload Requirements.(12.1.2)]
Verify that compressed files are checked for "zip bombs" - small input files
that will decompress into huge files thus exhausting file storage limits.

. [[r63]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V13.1 Generic Web Service Security Verification Requirements.(13.1.5)]
Verify that requests containing unexpected or missing content types are
rejected with appropriate headers
(*HTTP* response status **406 Unacceptable** or
**415 Unsupported Media Type**).

. [[r64]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V13.2 RESTful Web Service Verification Requirements.(13.2.2)]
Verify that *JSON* schema validation is in place and verified before accepting
input.

. [[r65]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V13.3 SOAP Web Service Verification Requirements.(13.3.1)]
Verify that *XSD* schema validation takes place to ensure a properly formed
*XML* document,
followed by validation of each input field before any processing of that data
takes place.

. [[r66]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 1.3.3]
Implement anti-spoofing measures to detect and block forged source IP addresses
from entering the network.

. [[r67]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.5.1]
Address common coding vulnerabilities in software-development processes such as
injection flaws, particularly *SQL* injection.
Also consider *OS* Command Injection, *LDAP* and *XPath* injection flaws as
well as other injection flaws.

. [[r68]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.5.2]
Address common coding vulnerabilities in software-development processes such as
buffer overflows.

. [[r69]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.5.7]
Address common coding vulnerabilities in software-development processes such as
cross-site scripting (*XSS*).
