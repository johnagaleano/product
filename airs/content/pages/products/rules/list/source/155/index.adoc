:slug: products/rules/list/155/
:category: source
:description: This requirement establishes the importance of having an application free of malicious code mainly developed by third parties.
:keywords: Source Code, Functional Requirements, ASVS, CAPEC, CWE, ISO, NERC, Different, Backdoors, Rules, Ethical Hacking, Pentesting
:rules: yes

= R155. Application free of malicious code

== Requirement

The application code must be free of malicious code.

== Description

There are several ways in which malicious code may be included in an
application.
It can be imported as part of third party libraries,
which may be intentionally malicious or have exploitable vulnerabilities,
or it can come as a backdoor left by one of the developers.
Therefore, the source code should be audited to guarantee it does not have any
backdoors, rootkits, time bombs, logic bombs, etc.

== References

. [[r1]] link:https://www.bsimm.com/framework/software-security-development-lifecycle/code-review.html[BSIMM9 CR3.4: 2. Automate malicious code detection].
Automated code review is used to identify dangerous code written by malicious
in-house developers or outsource providers.

. [[r2]] link:http://capec.mitre.org/data/definitions/438.html[CAPEC-438: Modification During Manufacture].
An attacker modifies a technology, product, or component during a stage in its
manufacture for the purpose of carrying out an attack against some entity
involved in the supply chain lifecycle.

. [[r3]] link:https://cwe.mitre.org/data/definitions/507.html[CWE-507: Trojan Horse].
The software appears to contain benign or useful functionality,
but it also contains code that is hidden from normal operation that violates
the intended security policy of the user or the system administrator.

. [[r4]] link:https://cwe.mitre.org/data/definitions/510.html[CWE-510: Trapdoor].
A trapdoor is a hidden piece of code that responds to a special input,
allowing its user access to resources without passing through the normal
security enforcement mechanism.

. [[r5]] link:https://cwe.mitre.org/data/definitions/511.html[CWE-511: Logic/Time Bomb].
The software contains code that is designed to disrupt the legitimate operation
of the software (or its environment) when a certain time passes,
or when a certain logical condition is met.

. [[r6]] link:https://www.iso.org/obp/ui/#iso:std:54534:en[ISO 27001:2013. Annex A - 12.2.1]
Establish detection, prevention and recovery mechanisms to protect against
malicious code.

. [[r7]] link:https://www.nerc.com/pa/Stand/Reliability%20Standards/CIP-007-6.pdf[NERC CIP-007-6. B. Requirements and measures. R3.1]
Deploy method(s) to deter, detect, or prevent malicious code.

. [[r8]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
Appendix C: Internet of Things Verification Requirements.(C.13)]
Verify all code including third-party binaries, libraries, frameworks are
reviewed for hardcoded credentials (*backdoors*).

. [[r9]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V10.2 Malicious Code Search.(10.2.1)]
Verify that the application source code and third party libraries do not
contain unauthorized phone home or data collection capabilities.
Where such functionality exists, obtain the user's permission for it to operate
before collecting any data.

. [[r10]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V10.2 Malicious Code Search.(10.2.3)]
Verify that the application source code and third party libraries do not contain
backdoors,
such as hard-coded or additional undocumented accounts or keys,
code obfuscation, undocumented binary blobs, rootkits,
or anti-debugging, insecure debugging features,
or otherwise out of date, insecure, or hidden functionality that could be used
maliciously if discovered.

. [[r11]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V10.2 Malicious Code Search.(10.2.4)]
Verify that the application source code and third party libraries do not
contain time bombs by searching for date and time related functions.

. [[r12]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V10.2 Malicious Code Search.(10.2.5)]
Verify that the application source code and third party libraries do not
contain malicious code, such as salami attacks, logic bypasses, or logic bombs.

. [[r13]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V10.2 Malicious Code Search.(10.2.6)]
Verify that the application source code and third party libraries do not
contain Easter eggs or any other potentially unwanted functionality.
