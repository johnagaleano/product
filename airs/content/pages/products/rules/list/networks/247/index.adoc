:slug: products/rules/list/247/
:category: networks
:description: This requirement establishes the importance of hiding the SSID for private networks that manage sensitive information.
:keywords: Wireless, Private Network, SSID, Hide, Security, Configuration, Rules, Ethical Hacking, Pentesting
:rules: yes

= R247. Hide SSID on private networks

== Requirement

The private networks must disable
the disclosure of the `SSID` (Service Set Identifier).
