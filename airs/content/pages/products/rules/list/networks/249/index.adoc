:slug: products/rules/list/249/
:category: networks
:description: This requirement establishes the importance of correctly locating the network access points to ensure the wireless signal's correct distribution.
:keywords: Network, SSID, Access Point, Wireless, Location, Security, Rules, Ethical Hacking, Pentesting
:rules: yes

= R249. Locate access points

== Requirement

The access points must be placed in strategic locations,
allowing the network signal to reach only the authorized facilities.
