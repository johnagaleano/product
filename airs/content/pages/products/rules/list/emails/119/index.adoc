:slug: products/rules/list/119/
:category: emails
:description: This requirement establishes the importance of hiding the recipients when sending bulk emails to avoid business information disclosure.
:keywords: BCC, Mails, Recipient, Information, Disclosure, Bulk, Rules, Ethical Hacking, Pentesting
:rules: yes

= R119. Hide recipients

== Requirement

The *BCC* field must be used instead of the *TO* field when sending bulk
emails.

== Findings

* [inner]#link:/products/rules/findings/055/[F055. Insecure service configuration]#
