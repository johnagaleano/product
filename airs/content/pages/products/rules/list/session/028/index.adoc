:slug: products/rules/list/028/
:category: session
:description: This requirement establishes the importance of allowing users to end any active session they have.
:keywords: Session, Logout, Allow, ASVS, CWE, NIST, OWASP, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R028. Allow users to log out

== Requirement

The system must allow users to view and manually log out of any or all active
sessions and devices.

== Description

Session tokens have associated permissions that allow any actor who possesses
them to perform actions in a system.
If a user leaves a session open and loses access to the device on which it
resides,
anyone with access to the device will be able to use that session.
Therefore, the system should allow users to view and log out of any active
session.

== References

. [[r1]] link:https://cwe.mitre.org/data/definitions/613.html[CWE-613: Insufficient Session Expiration].
Insufficient Session Expiration is when a web site permits
an attacker to reuse old session credentials or session IDs for authorization.

. [[r2]] link:https://pages.nist.gov/800-63-3/sp800-63b.html[NIST 800-63B 7.1 Session Bindings]
Secrets used for session binding SHALL be erased or invalidated by the session
subject when the subscriber logs out.

. [[r3]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A2-Broken_Authentication[OWASP Top 10 A2:2017-Broken Authentication].
Application functions related to authentication and session management are
often implemented incorrectly,
allowing attackers to compromise passwords, keys, or session tokens,
or to exploit other implementation flaws to assume other users' identities
temporarily or permanently.

. [[r4]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V3.3 Session Logout and Timeout Requirements.(3.3.4)]
Verify that users are able to view and log out of any or all currently active
sessions and devices.

. [[r5]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V3.5 Token-based Session Management.(3.5.1)]
Verify the application does not treat *OAuth* and refresh tokens — on their
own — as the presence of the subscriber and allows users to terminate
trust relationships with linked applications.

. [[r6]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.5.10]
Address common coding vulnerabilities in software-development processes such as
broken authentication and session management.
