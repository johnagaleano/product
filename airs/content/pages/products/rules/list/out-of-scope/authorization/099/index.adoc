:slug: products/rules/list/099/
:category: authorization
:description: This requirement establishes the importance of setting physical controls for vehicles and people in the facilities' parking zones.
:keywords: Requirement, Security, Access Control, Parking, Vehicles, Physical Access, Rules, Ethical Hacking, Pentesting
:rules: yes
:extended: yes

= R099. Vehicles and people access control

== Requirement

The organization must establish controls
for vehicles and people access in parking zones.

== References

. [[r1]] link:https://www.law.cornell.edu/cfr/text/45/164.310[+HIPAA Security Rules+ 164.310(a)(1)]
Facility Access Controls:
Implement policies and procedures to limit physical access
to its electronic information systems and the facility or facilities
in which they are housed,
while ensuring that properly authorized access is allowed.

. [[r2]] link:https://www.law.cornell.edu/cfr/text/45/164.310[+HIPAA Security Rules+ 164.310(a)(2)(ii):]
Facility Security Plan: Implement policies and procedures
to safeguard the facility and the equipment therein
from unauthorized physical access, tampering, and theft

. [[r3]] link:https://www.law.cornell.edu/cfr/text/45/164.310[+HIPAA Security Rules+ 164.310(a)(2)(iii):]
Access Control and Validation Procedures: Implement procedures
to control and validate a person's access to facilities
based on their role or function, including visitor control,
and control of access to software programs for testing and revision.
