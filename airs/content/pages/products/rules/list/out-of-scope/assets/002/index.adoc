:slug: products/rules/list/002/
:category: assets
:description: This requirement establishes the importance of identifying all dependencies and components used in information systems.
:keywords: Requirement, Security, Dependencies, Components, Identification, System, Rules, Ethical Hacking, Pentesting
:rules: yes
:extended: yes

= R002. Identify dependencies or components

== Requirement

All dependencies or components
used by information systems must be identified.
