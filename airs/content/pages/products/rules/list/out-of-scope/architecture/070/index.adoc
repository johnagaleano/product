:slug: products/rules/list/070/
:category: architecture
:description: This requirement establishes the importance of defining some automated security testing as part of the deployment process.
:keywords: Requirement, Security, Architecture, Automated, Testing, GDPR, Rules, Ethical Hacking, Pentesting
:rules: yes
:extended: yes

= R070. Define automated security testing

== Requirement

There must be a set of automated security tests
that run as part of the deployment process
(e.g., unit, integration, functional).

== Description

System configuration is essential when it comes to security issues.
The system must follow the industry's standard configurations that prevent
all known vulnerabilities.
As part of these configurations,
it should also include a set of tests that asses the conservation of the
security settings,
and that help prevent the inclusion of new insecure functionalities.


== References

. [[r1]] link:https://gdpr-info.eu/art-32-gdpr/[GDPR. Art. 32: Security of processing.(1)(d).]
The controller and the processor shall implement appropriate technical and
organizational measures to ensure an appropriate level of security,
including a process for regularly testing, assessing and evaluating the
effectiveness of technical and organizational measures for ensuring the
security of the processing.

. [[r2]] link:https://www.law.cornell.edu/cfr/text/45/164.312[+HIPAA Security Rules+ 164.312(c)(2):]
Mechanism to Authenticate Electronic Protected Health Information:
Implement electronic mechanisms to corroborate
that electronic protected health information
has not been altered or destroyed in an unauthorized manner.
