:slug: products/rules/list/063/
:category: architecture
:description: This requirement establishes the importance of verifying the security requirements defined for a system through white box tests.
:keywords: Security, Requirements, White Box, Testing, Verify, System, Rules, Ethical Hacking, Pentesting
:rules: yes
:extended: yes

= R063. Verify security requirements

== Requirement

The security requirements defined for a system
must be verifiable (+white box+ testing).
