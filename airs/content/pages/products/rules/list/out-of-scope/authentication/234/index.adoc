:slug: products/rules/list/234/
:category: authentication
:description: This requirement establishes the importance of protecting credentials with critical business information under the custody of two users.
:keywords: Requirement, Security, Credential, Business, Information, Users, Rules, Ethical Hacking, Pentesting
:rules: yes
:extended: yes

= R234. Protect authentication credentials

== Requirement

Access credentials for systems with critical business information must be
guarded by two authorized users.
