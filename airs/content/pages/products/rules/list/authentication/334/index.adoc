:slug: products/rules/list/334/
:category: authentication
:description: This requirement states that password hints and knowledge-based authentication mechanisms must not be present in the system.
:keywords: Knowledge, Based, Authentication, ASVS, CWE, NIST, OWASP, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R334. Avoid knowledge-based authentication

== Requirement

Password hints and knowledge-based authentication,
such as **secret questions**,
should not be enabled.

== Description

Password hints often offer enough information for an attacker to guess a user's
password.
Answers to **secret questions** are sometimes information that is publicly
available or that can be found on social media.
Therefore, these mechanisms should not be part of the authentication process
and should not be used in the password recovery process either.

== References

. [[r1]] link:https://cwe.mitre.org/data/definitions/640.html[CWE-640: Weak Password Recovery Mechanism for Forgotten Password]
The software contains a mechanism for users to recover or change their
passwords without knowing the original password,
but the mechanism is weak.

. [[r2]] link:https://pages.nist.gov/800-63-3/sp800-63b.html[NIST 800-63B 5.1.1.2 Memorized Secret Verifiers]
Memorized secret verifiers SHALL NOT permit the subscriber to store a “hint”
that is accessible to an unauthenticated claimant.
Verifiers SHALL NOT prompt subscribers to use specific types of information
(e.g., “What was the name of your first pet?”) when choosing memorized secrets.

. [[r3]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A2-Broken_Authentication[OWASP Top 10 A2:2017-Broken Authentication].
Application functions related to authentication and session management are
often implemented incorrectly,
allowing attackers to compromise passwords, keys, or session tokens,
or to exploit other implementation flaws to assume other users' identities
temporarily or permanently.

. [[r4]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V2.5 Credential Recovery Requirements.(2.5.2)]
Verify password hints or knowledge-based authentication
(so-called "secret questions") are not present.

. [[r5]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.5.10]
Address common coding vulnerabilities in software-development processes such as
broken authentication and session management.
