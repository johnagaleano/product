:slug: products/rules/list/095/
:category: authorization
:description: This requirement establishes the importance of defining the users with administrator or root privileges in the system.
:keywords: Users, Privileges, Root, Administrator, ASVS, CAPEC, CWE, HIPAA, NIST, OWASP, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R095. Define users with privileges

== Requirement

The users that will access the system with administrator or root
privileges must be defined.

== Description

Systems should have a set of roles with different levels
of privilege to access resources.
The privileges of each role must be clearly defined and the role of each user
should also be clearly stated.
That includes the set of users that will have administrator or root privileges,
as this should not be a default role.

== Findings

* [inner]#link:/products/rules/findings/031/[F031. Excessive privileges]#

== References

. [[r1]] link:http://capec.mitre.org/data/definitions/122.html[CAPEC-122: Privilege Abuse].
An adversary is able to exploit features of the target that should be reserved
for privileged users or administrators but are exposed to use by lower or
non-privileged accounts.
Access to sensitive information and functionality must be controlled to ensure
that only authorized users are able to access these resources.

. [[r2]] link:http://capec.mitre.org/data/definitions/233.html[CAPEC-233: Privilege Escalation].
An adversary exploits a weakness enabling them to elevate their privilege and
perform an action that they are not supposed to be authorized to perform.

. [[r3]] link:https://www.cisecurity.org/controls/[CIS Controls. 4.1 Maintain Inventory of Administrative Accounts].
Use automated tools to inventory all administrative accounts,
including domain and local accounts, to ensure that only authorized individuals
have elevated privileges.

. [[r4]] link:https://cwe.mitre.org/data/definitions/250.html[CWE-250: Execution with Unnecessary Privileges]
The software performs an operation at a privilege level that is higher than the
minimum level required,
which creates new weaknesses or amplifies the consequences of other weaknesses.

. [[r5]] link:https://cwe.mitre.org/data/definitions/276.html[CWE-276: Incorrect Default Permissions].
The product, upon installation, sets incorrect permissions for an object that
exposes it to an unintended actor.

. [[r6]] link:https://www.law.cornell.edu/cfr/text/45/164.308[HIPAA Security Rules 164.308(a)(3)(i):]
Workforce Security: Implement policies and procedures
to ensure that all members of its workforce have appropriate access
to electronic protected health information,
as provided under paragraph (a)(4) of this section,
and to prevent those workforce members who do not have access
under paragraph (a)(4) of this section
from obtaining access to electronic protected health information.

. [[r7]] link:https://www.law.cornell.edu/cfr/text/45/164.310[HIPAA Security Rules 164.310(a)(2)(iii):]
Access Control and Validation Procedures: Implement procedures
to control and validate a person's access to facilities
based on their role or function, including visitor control,
and control of access to software programs for testing and revision.

. [[r8]] link:https://nvd.nist.gov/800-53/Rev4/control/AC-2[NIST 800-53 AC-2 (6)]
The information system implements the following
dynamic privilege management capabilities:
[Assignment: organization-defined list
of dynamic privilege management capabilities].

. [[r9]] link:https://nvd.nist.gov/800-53/Rev4/control/AC-2[NIST 800-53 AC-2 (7) a]
The organization establishes and administers privileged user accounts
in accordance with a role-based access scheme
that organizes allowed information system access and privileges into roles.

. [[r10]] link:https://nvd.nist.gov/800-53/Rev4/control/AC-2[NIST 800-53 AC-2 (7) b]
The organization monitors privileged role assignments.

. [[r11]] link:https://nvd.nist.gov/800-53/Rev4/control/AC-2[NIST 800-53 AC-2 (7) c]
The organization Takes [Assignment: organization-defined actions]
when privileged role assignments are no longer appropriate.

. [[r12]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A5-Broken_Access_Control[OWASP Top 10 A5:2017-Broken Access Control].
Restrictions on what authenticated users are allowed to do are often not
properly enforced.
Attackers can exploit these flaws to access unauthorized functionality and/or
data, such as access other users' accounts, view sensitive files,
modify other users' data, change access rights, etc.

. [[r13]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V4.1 General Access Control Design.(4.1.4)]
Verify that the principle of deny by default exists whereby new users/roles
start with minimal or no permissions and users/roles do not receive access to
new features until access is explicitly assigned.

. [[r14]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.5.8]
Address common coding vulnerabilities in software-development processes
including improper access control
(such as insecure direct object references, failure to restrict URL access,
directory traversal, and failure to restrict user access to functions).

. [[r15]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 7.1.1]
Define access needs for each role,
including level of privilege required (for example, user, administrator, etc.)
for accessing resources.

. [[r16]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 7.1.3]
Assign access based on individual personnel's job classification and function.

. [[r17]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 7.2.2]
This access control system(s) must include assignment of privileges to
individuals based on job classification and function.
