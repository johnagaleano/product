:slug: products/rules/list/134/
:category: credentials
:description: This requirement establishes the importance of storing passwords securely using cryptographic functions to mask their content.
:keywords: Passwords, Hash, Salt, ASVS, CWE, NIST, Rules, Ethical Hacking, Pentesting
:rules: yes

= R134. Store passwords with salt

== Requirement

The system must store passwords
with different key derivations (*Salt*).

== Findings

* [inner]#link:/products/rules/findings/020/[F020. Non-encrypted confidential information]#

* [inner]#link:/products/rules/findings/051/[F051. Cracked weak credentials]#

== References

. [[r1]] link:https://www.cisecurity.org/controls/[CIS Controls. 16.4 Encrypt or Hash All Authentication Credentials].
Encrypt or hash with a salt all authentication credentials when stored.

. [[r2]] link:https://cwe.mitre.org/data/definitions/759.html[CWE-759: Use of a One-Way Hash without a Salt].
The software uses a one-way cryptographic hash against an input that should not
be reversible, such as a password,
but the software does not also use a salt as part of the input.

. [[r3]] link:https://cwe.mitre.org/data/definitions/760.html[CWE-760: Use of a One-Way Hash with a Predictable Salt].
The software uses a one-way cryptographic hash against an input that should not
be reversible, such as a password,
but the software uses a predictable salt as part of the input.

. [[r4]] link:https://cwe.mitre.org/data/definitions/916.html[CWE-916: Use of Password Hash With Insufficient Computational Effort].
The software generates a hash for a password,
but it uses a scheme that does not provide a sufficient level of computational
effort that would make password cracking attacks infeasible or expensive.

. [[r5]] link:https://pages.nist.gov/800-63-3/sp800-63b.html[NIST 800-63B 5.1.1.2 Memorized Secret Verifiers]
Memorized secrets SHALL be salted and hashed using a suitable one-way key
derivation function.

. [[r6]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V2.4 Credential Storage Requirements.(2.4.1)]
Verify that passwords are stored in a form that is resistant to offline
attacks.
Passwords SHALL be salted and hashed using an approved one-way key derivation
or password hashing function.
Key derivation and password hashing functions take a password, a salt,
and a cost factor as inputs when generating a password hash.
