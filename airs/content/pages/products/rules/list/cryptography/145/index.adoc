:slug: products/rules/list/145/
:category: cryptography
:description: This requirement establishes the importance of protecting system cryptographic keys.
:keywords: Asymmetric, Symmetric, Cryptography, Keys, ASVS, CWE, PCI DSS, Rules, Ethical Hacking, Pentesting
:rules: yes

= R145. Protect system cryptographic keys

== Requirement

The system's private asymmetric or symmetric keys must be protected
and should not be exposed.

== Description

The system's cryptographic keys are essential for maintaining the confidentiality
and integrity of transactions and communications.
Their exposure may cause business information leakages, loss of data integrity
and loss of trust due to the inability to differentiate server traffic from
malicious traffic.
Therefore, these keys must be protected and managed following industry-verified
standards.

== Findings

* [inner]#link:/products/rules/findings/009/[F009. Sensitive information in source code]#

== References

. [[r1]] link:https://cwe.mitre.org/data/definitions/321.html[CWE-321: Use of Hard-coded Cryptographic Key]
The use of a hard-coded cryptographic key significantly increases the
possibility that encrypted data may be recovered.

. [[r2]] link:https://cwe.mitre.org/data/definitions/322.html[CWE-322: Key Exchange without Entity Authentication]
The software performs a key exchange with an actor without verifying the
identity of that actor.

. [[r3]] link:https://cwe.mitre.org/data/definitions/323.html[CWE-323: Reusing a Nonce, Key Pair in Encryption]
Nonces should be used for the present occasion and only once.

. [[r4]] link:https://cwe.mitre.org/data/definitions/522.html[CWE-522: Insufficiently Protected Credentials]
The product transmits or stores authentication credentials,
but it uses an insecure method that is susceptible to unauthorized interception
and/or retrieval.

. [[r5]] link:https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A3-Sensitive_Data_Exposure[OWASP Top 10 A3:2017-Sensitive Data Exposure].
Many web applications and **API**s do not properly protect sensitive data,
such as financial, healthcare, and *PII*.
Attackers may steal or modify such weakly protected data to conduct credit card
fraud, identity theft, or other crimes.
Sensitive data may be compromised without extra protection,
such as encryption at rest or in transit, and requires special precautions when
exchanged with the browser.

. [[r6]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
Appendix C: Internet of Things Verification Requirements.(C.6)]
Verify that sensitive data, private keys and certificates are stored securely
in a Secure Element, *TPM*, *TEE* (Trusted Execution Environment),
or protected using strong cryptography.

. [[r7]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V1.6 Cryptographic Architectural Requirements.(1.6.1)]
Verify that there is an explicit policy for management of cryptographic keys
and that a cryptographic key lifecycle follows a key management standard such
as **NIST SP 800-57**.

. [[r8]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V1.6 Cryptographic Architectural Requirements.(1.6.2)]
Verify that consumers of cryptographic services protect key material and other
secrets by using key vaults or API based alternatives.

. [[r9]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V1.6 Cryptographic Architectural Requirements.(1.6.3)]
Verify that all keys and passwords are replaceable and are part of a
well-defined process to re-encrypt sensitive data.

. [[r10]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V1.6 Cryptographic Architectural Requirements.(1.6.4)]
Verify that symmetric keys, passwords, or API secrets generated by or shared
with clients are used only in protecting low risk secrets,
such as encrypting local storage, or temporary ephemeral uses such as parameter
obfuscation.

. [[r11]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V2.8 Single or Multi Factor One Time Verifier Requirements.(2.8.2)]
Verify that symmetric keys used to verify submitted **OTP**s are highly
protected,
such as by using a hardware security module or secure operating system based
key storage.

. [[r12]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V2.9 Cryptographic Software and Devices Verifier Requirements.(2.9.1)]
Verify that cryptographic keys used in verification are stored securely
and protected against disclosure,
such as using a *TPM* or *HSM*, or an *OS* service that can use this secure
storage.

. [[r13]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V6.4 Secret Management.(6.4.1)]
Verify that a secrets management solution such as a key vault is used to
securely create, store, control access to and destroy secrets.

. [[r14]] link:https://owasp.org/www-project-application-security-verification-standard/[OWASP-ASVS v4.0.1
V6.4 Secret Management.(6.4.2)]
Verify that key material is not exposed to the application but instead uses an
isolated security module like a vault for cryptographic operations.

. [[r15]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 3.4.1]
Decryption keys must not be associated with user accounts.

. [[r16]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 3.5.2]
Restrict access to cryptographic keys to the fewest number of custodians
necessary.

. [[r17]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 3.5.3]
Store secret and private keys used to encrypt/decrypt cardholder data within a
secure cryptographic device (such as a hardware (host) security module (*HSM*)
or **PTS**-approved point-of-interaction device).

. [[r18]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 3.5.4]
Store cryptographic keys in the fewest possible locations.

. [[r19]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 3.6.2]
Fully document and implement all key-management processes and procedures for
cryptographic keys including secure cryptographic key distribution.

. [[r20]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 3.6.3]
Fully document and implement all key-management processes and procedures for
cryptographic keys including secure cryptographic key storage.

. [[r21]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 3.6.7]
Fully document and implement all key-management processes and procedures for
cryptographic keys including prevention of unauthorized substitution of
cryptographic keys.

. [[r22]] link:https://www.pcisecuritystandards.org/documents/PCI_DSS_v3-2-1.pdf[PCI DSS v3.2.1 - Requirement 6.5.3]
Address common coding vulnerabilities in software-development processes such as
insecure cryptographic storage.
