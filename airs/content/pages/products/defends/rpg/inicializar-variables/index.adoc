:slug: products/defends/rpg/inicializar-variables/
:category: rpg
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la gestión segura de variables dentro de un programa desarrollado en RPG, en este ocasión se explica la manera de inicializar explícitamente todas las variables declaradas dentro de un programa RPG.
:keywords: RPG, Recursos, Inicializar, Variables, Declarar, Tiempo De Vida.
:defends: yes

= Inicializar Variables

== Necesidad

Inicializar variables en +RPG+

== Contexto

A continuación se describe las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Las variables del código fuente
deben estar inicializadas de forma explícita<<r1,^[1]^>>.

== Solución

. Los datos son fragmentos de información sobre los cuales el programa opera,
estos fragmentos pueden ser manejados a través de variables o constantes.

. Las variables contienen valores
que pueden cambian durante la ejecución del programa,
mientras que las constantes no.

. Existen 15 tipos de datos en el lenguaje de programación +RPG+:
+
.Tipos de datos en +RPG+.
[options="header"]
|====
|Tipo |Unidad de almacenamiento |Tipos de datos que puede representar

|A
|1 a 16,773,104 +bytes+ (fijo)
1 a 16,773,100 +bytes+ (variable)
|Caracteres alfanuméricos

|B
|1 +byte+ (+8-bit+)
2 +bytes+ (+16-bit+)
4 +bytes+ (+32-bit+)
8 +bytes+ (+64-bit+)
|Número entero binario con signo

|C
|1 a 8,386,552 caracteres (fijo)
1 a 8,386,550 caracteres (variable) +16-bit+
|Caracteres +UCS-2+ (+DBCS+ o +EGCS+)

|D
|10 +bytes+
|Fecha: año, mes, día

|F
|4 +bytes+ (+32-bit+)
8 +bytes+ (+64-bit+)
|Número real de punto flotante con signo

|G
|1 a 8,386,552 caracteres (fijo)
1 a 8,386,550 caracteres (variable) +16-bit+
|Caracteres gráficos (+DBCS+ or +EGCS+)

|I
|1 +byte+ (+8-bit+)
2 +bytes+ (+16-bit+)
4 +bytes+ (+32-bit+)
8 +bytes+ (+64-bit+)
|Número entero con signo

|N
|1 +byte+
|Indicador con dos posibles valores:
'1' = Verdadero
'0' = Falso

|O
|-
|Referencia a objeto

|P
|1 a 63 dígitos, 2 dígitos por +byte+ más el signo
|Número decimal de punto flotante con parte entera y decimal

|S
|1 a 63 dígitos, 1 dígito por +byte+
|Número decimal de punto flotante con parte entera y decimal

|T
|8 +bytes+
|Tiempo: hora, minuto, segundo

|U
|1 +byte+ (+8-bit+)
2 +bytes+ (+16-bit+)
4 +bytes+ (+32-bit+)
8 +bytes+ (+64-bit+)
|Número entero sin signo

|Z
|26 +bytes+
|Fecha y tiempo: año, mes, día, hora, minuto, segundo, microsegundos

|*
|16 +bytes+
|Puntero a datos
Puntero a procedimientos activos
Puntero a objetos

|====

. Todas las variables deben ser inicializadas explícitamente
en el momento de su declaración
para evitar problemas de seguridad
e inconsistencias en el flujo del programa.
A continuación puede observar
la forma de inicializar diferentes tipos de variables
en el lenguaje de programación +RPG+:

* Las variables pueden ser inicializadas explícitamente<<r2,^[2]^>>
en el momento de su declaración
en la especificación de las definiciones
(Posición 6 con el carácter +D+),
a través de la palabra clave +INZ+:
+
.test.v
[source, verilog, linenums]
----
     DTexto            S             10A   INZ('Caracteres')
     DFecha            S               D   INZ(d'2013-07-04')
     DNumeroReal       S              8F   INZ(31337.29)
     DNumeroEntero     S             20I 0 INZ(123456789)

      /free
       DSPLY Texto;
       DSPLY Fecha;
       DSPLY NumeroReal;
       DSPLY NumeroEntero;

       *inlr = *on;
      /end-free
----

== Descargas

Puedes descargar el código fuente
pulsando en el siguiente enlace:

. [button]#link:src/definition.rpg[definition.rpg]# contiene
las declaraciones de variables descritas anteriormente.

== Referencias

. [[r1]] link:../../../products/rules/list/168/[REQ.168 Variables inicializadas explícitamente].
. [[r2]] link:http://www.rpgpgm.com/2014/02/defining-variables-in-rpg-all-free.html[Defining variables in RPG all free].
