      *** START HEADER FILE SPECIFICATIONS ****************************
      *
      *Header File Name: QPHONE.CBLINC
      * LANGUAGE:     COBOL                                             *
      *
      *Descriptive Name: Estructura de datos para registros telefonicos
      *
      *Description: Estructura reutilizable para registros telefonicos
      *
      *** END HEADER FILE SPECIFICATIONS ******************************

       01 PHONE-RECORD.
           05 PHONE-LAST-NAME  PIC X(10).
           05 PHONE-FIRST-NAME PIC X(10).
           05 PHONE-NUMBER     PIC X(10).
