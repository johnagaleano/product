:slug: products/defends/cobol/validar-correo-electronico/
:category: cobol
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en COBOL al explicar cómo se debe validar una cuenta de correo electrónico. La validación de información es una tarea clave en cualquier aplicación, para evitar problemas de seguridad.
:keywords: Validar, Correo electrónico, COBOL, Email, Dominio, Usuario.
:defends: yes

= Validar Correo Electrónico

== Necesidad

Validar correo electrónico en +COBOL+.

== Contexto

A continuación se describe las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se está desarrollando una aplicación en +COBOL+.
. Debe validarse que el correo electrónico
se encuentra asociado a un solo usuario
a pesar del componente entre el +++ y la +@+<<r1,^[1]^>>.

== Solución

. Se define la división +IDENTIFICATION DIVISION+<<r2,^[2]^>> para la solución:
+
.cobolemail.cbl
[source,cobol,linenums]
----
       IDENTIFICATION DIVISION.
      ******************
      * Identification *
      ******************
       PROGRAM-ID. COBOLEMAIL.
----
. En la división +DATA DIVISION+<<r3,^[3]^>>
se definen las variables
que serán usadas por el programa:

* +W01-EMAIL:+ contiene el email ingresado por entrada estándar.

* +W02-VALIDEMAIL:+ al final del proceso contendrá el correo electrónico final
que se debe usar en producción
(Correo sin el componente entre +++ y +@+).

* +W03-COUNT:+ variable temporal usada para contar diferentes caracteres.

* +W04-EMAIL:+ variable temporal para almacenar el nombre de usuario
y el dominio del correo electrónico proporcionado.
+
[source,cobol,linenums]
----
      ********
      * Data *
      ********
       DATA DIVISION.

       WORKING-STORAGE SECTION.
       01 W01-EMAIL      PIC X(256) VALUE "".
       01 W02-VALIDEMAIL PIC X(256) VALUE "".
       01 W03-COUNT      PIC 9(002) VALUE 0.
       01 W04-EMAIL
           02 W04-USERNAME PIC X(64).
           02 W04-DOMAIN   PIC X(255).
----
. Iniciamos nuestro programa dentro de la división +PROCEDURE DIVISION+<<r4,^[4]^>>
aceptando por entrada estándar un correo electrónico:
+
[source,cobol,linenums]
----
      ********
      * Main *
      ********
       PROCEDURE DIVISION.
       MAIN.
           ACCEPT W01-EMAIL.
----
. Contamos todos los arrobas que se encuentran en el correo proporcionado:
+
[source,cobol,linenums]
----
       INSPECT W01-EMAIL
       TALLYING W03-COUNT
       FOR ALL "@".
----
. El número de arrobas debe ser mayor a cero:
+
[source,cobol,linenums]
----
       IF W03-COUNT > 0 THEN
----
. Reinicializamos la variable de contador:
+
[source,cobol,linenums]
----
       MOVE 0 TO W03-COUNT
----
. Separamos el correo electrónico a través del arroba,
en la primera parte
debe quedar el nombre de usuario
y en la segunda parte el dominio:
+
[source,cobol,linenums]
----
       UNSTRING W01-EMAIL
       DELIMITED BY "@"
       INTO W04-USERNAME W04-DOMAIN
----
. Contamos todos los caracteres en el nombre del usuario:
+
[source,cobol,linenums]
----
       INSPECT W04-USERNAME
       TALLYING W03-COUNT
       FOR CHARACTERS
       BEFORE INITIAL SPACE
----
. Si es mayor a cero,
reinicializamos el contador y continuamos con el proceso:
+
[source,cobol,linenums]
----
       IF W03-COUNT > 0 THEN
           MOVE 0 TO W03-COUNT
----
. Se cuenta todos los caracteres en la parte del dominio (sin incluir espacios):
+
[source,cobol,linenums]
----
       INSPECT W04-DOMAIN
       TALLYING W03-COUNT
       FOR CHARACTERS
       BEFORE INITIAL SPACE
----
. Si contiene un nombre de dominio,
procedemos a realizar la última parte del proceso:
+
[source,cobol,linenums]
----
       IF W03-COUNT > 0 THEN
----
. Enviamos el componente antes del carácter +++
a la variable +W02-VALIDEMAIL:+
+
[source,cobol,linenums]
----
       UNSTRING W04-USERNAME
       DELIMITED BY "+"
       INTO W02-VALIDEMAIL
----
. Por último unimos el usuario con el nombre del dominio,
de esta forma separamos el componente entre el +++ y el +@+,
evitando que un usuario pueda registrar múltiples cuentas
con un único correo válido:
+
[source,cobol,linenums]
----
       STRING W02-VALIDEMAIL "@" W04-DOMAIN
       DELIMITED BY SPACE
       INTO W02-VALIDEMAIL
----
. Mostramos el correo electrónico válido:
+
[source,cobol,linenums]
----
       DISPLAY W02-VALIDEMAIL
----
. Cualquier otra situación puede ser considerada
como un error de correo electrónico:
+
[source,cobol,linenums]
----
               ELSE
                   PERFORM EMAIL-ERROR
               END-IF
           ELSE
               PERFORM EMAIL-ERROR
           END-IF
       ELSE
           PERFORM EMAIL-ERROR
       END-IF.

       STOP RUN.
----
. Para el ejemplo ilustrativo, se muestra un mensaje de error.
En esta sección debe ser tratado el error:
+
[source,cobol,linenums]
----
       EMAIL-ERROR.
           DISPLAY "Email error".
           STOP RUN.
----
. Estas son algunas de las pruebas realizadas con el programa:
+
.Salida obtenida luego de ejecutar el programa para diferentes datos de entrada.
[options="header"]
|===
|Correo electrónico |Correo electrónico válido

|admin
|Error

|admin@
|Error

|@domain.com
|Error

|admin@domain.com
|admin@domain.com

|admin+1234@domain.com
|admin@domain.com

|===

== Descargas

Puedes descargar el código fuente
pulsando en el siguiente enlace:

. [button]#link:src/cobolemail.cbl[cobolemail.cbl]# contiene
las validaciones de correo electrónico explicadas anteriormente.

== Referencias

. [[r1]] link:../../../products/rules/list/121/[REQ.121 Garantizar unicidad de correos].
. [[r2]] link:https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_73/rzasb/iddiv.htm[Identification Division].
. [[r3]] link:http://www.escobol.com/modules.php?name=Sections&op=viewarticle&artid=13[Data Division].
. [[r4]] link:https://www.ibm.com/support/knowledgecenter/SSQ2R2_9.1.1/com.ibm.ent.cbl.zos.doc/PGandLR/ref/rlpds.html[Procedure division structure].
