:slug: products/defends/aspnet/remover-metadata-imgs/
:category: aspnet
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en ASP.NET al remover los metadatos de las imágenes. Los metadatos pueden contener información sensible, por lo que es altamente recomendable eliminarlos.
:keywords: ASP.NET, Remover, Metadatos, Imágenes, Seguridad, Información Sensible.
:defends: yes

= Remover Metadatos de Imágenes

== Necesidad

Remover metadatos de imágenes en +ASP.NET+.

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se está desarrollando una aplicación en +ASP.NET+.
. La aplicación utiliza imágenes o fotografías.
. Se requiere remover metadatos en imágenes.

== Solución

Al momento de desarrollar una aplicación,
es importante que tanto ésta como sus dependencias,
cumplan con ciertas medidas de seguridad
para garantizar que no sea vulnerada por ciberdelincuentes.
Cuando una aplicación utiliza imágenes
a menudo éstas contienen metadatos.
Se define como metadato a la información adicional
que se encuentra dentro de la imagen,
la cual es fácilmente accesible,
pero no está a plena vista sino dentro de las propiedades de ésta.
Para el caso de las fotografías,
los metadatos pueden contener información sobre el dispositivo
desde el cual fue tomada como su marca y modelo,
la fecha y hora exactas de la fotografía,
así como su ubicación geográfica <<r1, ^[1]^>>,
lo cual representa un grave riesgo
para la confidencialidad de la información.
Es por esto que al momento de trabajar con imágenes
es altamente recomendable eliminar los metadatos,
para ello, debemos seguir los siguientes pasos:

. Para eliminar metadatos en imágenes,
utilizaremos la librería +System.Drawing+
utilizada para el tratamiento de imágenes.
Primero debemos importar los +namespaces+ necesarios
y definimos el nombre de la clase.
Dado que es una librería del sistema,
únicamente utilizamos las librerías +System+  y +System.Drawing+,
como se muestra a continuación:
+
.imgcleaning.cs
[source, csharp, linenums]
----
using System;
using System.Drawing;

namespace metadata
{
  class MainClass
  {
----

. El método utilizado para remover los metadatos es +RemoveExifData+.
Esté método recibe como parámetro la ruta de la imagen a modificar,
la cual definiremos en el +Main+ de nuestro programa,
como se muestra a continuación:
+
[source,csharp,linenums]
----
  public static void Main(string[] args) {
    try {
      Console.WriteLine(RemoveExifData("image.jpg"));
    }
    catch (IOException e) {
      Console.WriteLine("Error "+e.Message);
    }
  }
----

. El método +RemoveExifData+ se encarga de leer la imagen pixel por pixel,
sin tener en cuenta cualquier otro tipo de dato,
por lo que es una función segura al momento de eliminar información extra
contenida en la imagen.
+
[source, csharp, linenums]
----
  public static string RemoveExifData(string imagePath) {
----

. Se lee la imagen y se vuelve a guardar,
para ello utilizamos el método +Save+
de la clase +Image+.
Al momento de volver a guardar la imagen,
ésta se sobrescribe, eliminando los metadatos:
+
[source, csharp, linenums]
----
    Image bufImage = Image.FromFile(imagePath);
    bufImage.Save(imagePath);
    return imagePath;
  }
----

== Referencias

. [[r1]] link:https://www.welivesecurity.com/la-es/2014/05/13/metadatos-fotos-podrian-mostrar-mas/[ESET image metadata].
. [[r2]] link:https://docs.microsoft.com/es-es/dotnet/framework/winforms/advanced/how-to-read-image-metadata[Microsoft .NET Cómo: Leer metadatos de imagen].
. [[r3]] link:../../../products/rules/list/045/[REQ.045 Eliminar metadatos al compartir archivos].
