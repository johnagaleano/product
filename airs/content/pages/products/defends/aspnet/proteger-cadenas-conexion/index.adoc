:slug: products/defends/aspnet/proteger-cadenas-conexion/
:category: aspnet
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la configuracion segura de cadenas de conexión en ASP.NET. Las cadenas de conexión contienen información crítica en una aplicación web, por lo que deben estar bien protegidas en todo momento.
:keywords: ASP.NET, Seguridad, Configuración, Cadenas de conexión, Cifrar, RSA.
:defends: yes

= Proteger cadenas de conexión

== Necesidad

Almacenar las cadenas de conexión
para el acceso a datos
en archivos de configuración de la aplicación.

== Contexto

A continuación se describe las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. La aplicación esta construida en +ASP.NET+ versión +1.1+, +2.0+, o superior.

. La aplicación requiere conectarse a una base de datos.

== Solución

La protección del acceso a la fuente de datos
es uno de los objetivos más importantes
cuando se asegura una aplicación.
Una cadena de conexión presenta una vulnerabilidad potencial
si no está asegurada.
Almacenar la información de la conexión
en texto plano o persistiendo ésta en memoria
puede comprometer la seguridad de la aplicación.
Las cadenas de conexión embebidas en el código fuente de la aplicación
pueden ser leídas usando la herramienta +Ildasm.exe+ (+MSIL Disassembler+),
la cual se puede emplear
para ver el lenguaje intermedio
de +Microsoft (MSIL)+ en un ensamblado compilado <<r1 ,^[1]^>>.
Para proteger la información de conexión
se debe realizar los siguientes pasos:

. Declarar la sección +connectionStrings+
dentro del elemento +configuration+
en el archivo +web.config+ de la aplicación
utilizando la siguiente parametrización:
+
.Web.config
[source,xml,linenums]
----
<?xml version='1.0' encoding='utf-8'?>
<configuration>
 <connectionStrings>
   <clear />
   <add name="Name"
   providerName="System.Data.ProviderName"
   connectionString="Valid Connection String;" />
 </connectionStrings>
</configuration>
----
+
Los atributos del subelemento +add+ son:
+
* +Name:+ Utilizado para especificar el nombre
de la cadena de conexión,
el cual es recuperado en tiempo de ejecución.
Este nombre debe ser único
para identificar una conexión en particular.

* +ProviderName:+ Es el nombre del proveedor de datos del +.NET Framework+
 que se usará para la conexión.

* +ConnectionString:+ Es el valor de la cadena de conexión
para una base de datos específica.

. Recuperar la cadena de conexión
declarada en el archivo de configuración
desde el código de la aplicación.
Para ello, se debe usar la clase +ConnectionStringSettings+
del nombre de espacio +System.Configuration+ <<r2,^[2]^>>.
+
[source,C,linenums]
----
string nombreConexion = null;
// Look for the name in the connectionStrings section.
ConnectionStringSettings settings =
ConfigurationManager.ConnectionStrings[name];
// If found, return the connection string.
if (settings != null)
   nombreConexion = settings.ConnectionString;
----

. Cifrar la sección +connectionStrings+.
Para cifrar la información de conexión
que contiene el elemento +connectionStrings+
en el archivo Web.config de la aplicación +ASP.NET+,
se debe usar la herramienta +Aspnet_regiis.exe+
con el siguiente comando <<r3,^[3]^>>:
+
[source, bat, linenums]
----
aspnet_regiis -pe "connectionStrings" -app "/AppName" -prov "RsaProtectedConfigurationProvider"
----

. Descifrar una sección del archivo de configuración.
Para descifrar la información de conexión
que contiene el elemento +connectionStrings+
en el archivo Web.config se debe realizar
utilizando el siguiente comando <<r3, ^[3]^>>:
+
[source, bat, linenums]
----
aspnet_regiis -pd "connectionStrings" -app "/AppName"
----

== Referencias

. [[r1]] link:https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/protecting-connection-information[Microsoft, Protecting Connection Information].

. [[r2]] link:https://msdn.microsoft.com/es-es/library/bf7sd233(VS.80).aspx[Microsoft, connectionStrings].

. [[r3]] link:https://msdn.microsoft.com/en-us/library/53tyfkaw.aspx[Microsoft, Encrypting Configuration Information
Using Protected Configuration].

. [[r4]] link:https://msdn.microsoft.com/en-us/library/zhhddkxy.aspx[Microsoft, Encrypting and Decrypting Configurations Sections].

. [[r5]] link:https://msdn.microsoft.com/es-es/library/k6h9cz8h(VS.90).aspx[Microsoft, Herramienta Registro de IIS].
