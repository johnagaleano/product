:slug: products/defends/aspnet/generar-identificador-unico/
:category: aspnet
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en ASPNET al generar un Identificador Único Universal (UUID). Los UUIDs son números de 32 dígitos virtualmente únicos que sirven para identificar de forma inequívoca un recurso o proceso.
:keywords: ASPNET, Seguridad, UUID, Identificador, Universal, Transacción.
:defends: yes

= Generar identificador único para transacción

== Necesidad

Generar un identificador universalmente único
para identificar inequívocamente una transacción o recurso.

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se pretende utilizar un método estándar
para la creación de identificadores únicos.

. La aplicación está desarrollada en +ASP.NET+.

. Los números aleatorios generados
deben seguir una distribución uniforme.<<r1,^[1]^>>

== Solución

El identificador único universal,
o +UUID+ por sus siglas en inglés,
es un estándar que especifica
como debe ser un número que pueda considerarse
para identificar 'inequívocamente' transacciones o recursos.
El término de único se refiere en realidad
a una baja probabilidad de crear dos números iguales.
Luego de generar 1 billón de +UUID+
cada segundo por los siguientes 100 años,
la probabilidad de crear uno que esté duplicado
sería cerca del 50%" <<r2,^[2]^>>.

Un +UUID+ está conformado por
5 grupos de dígitos en formato hexadecimal de
longitud 8-4-4-4-12 respectivamente separados por guiones,
para un total de 36 caracteres (32 dígitos y 4 guiones).
Un ejemplo de un +UUID+ en su forma canónica es es siguiente:

----
550e8400-e29b-41d4-a716-446655440000
----

Los +UUIDs+ tienen un campo de aplicación
bastante extenso en diferentes áreas.
Entre los ejemplos de aplicación de un UUID se cuenta:

* Identificación y reconocimiento de dispositivos.

* Operaciones electrónicas de índole fiscal y tributaria.

* Autenticación de documentos electrónicos.

* Procesos de construcción de Software.

El identificador global único o +GUID+,
es la implementación de +UUID+ de +Microsoft+.
El +GUID+ es utilizado para distinguir interfaces
de componentes software diferentes <<r3,^[3]^>>,
esto quiere decir que dos componentes
con un mismo nombre y versión,
puede mostrarse de forma diferente al usuario,
si tiene un +GUID+ diferente.

Un +GUID+ es tratado como un objeto,
cuando es insertado en programas de +Microsoft Office+,
pero también puede ayudar a identificar
flujos de audio y video.

. A continuación se muestra como generar un +GUID+ desde programación.
Para ello se utilizará el método +NewGuid+ de la librería +System+.
A continuación se muestra la implementación del método anteriormente descrito:
+
.test.py
[source, c, linenums]
----
using System;
namespace LineaBase
{
    class Program
    {
        static void Main(string[] args)
        {
            string uuid = System.Guid.NewGuid().ToString();
            Console.WriteLine(uuid);
        }
    }
}
----

. Como se observa, basta con llamar el método estático +NewGuid+
de la estructura +Guid+ <<r4,^[4]^>>.
Se pueden evidenciar diferentes identificadores
al compilar y ejecutar la aplicación:
+
[source,csharp,linenums]
----
> csc UUID.cs
Microsoft (R) Visual C# 2010 Compiler version 4.0.30319.1
Copyright (C) Microsoft Corporation. All rights reserved.

> UUID
2442a8c2-d366-49ff-bc37-11332a513d36

> UUID
00e2a461-8d9c-4463-a633-58af21181ecb

> UUID
063b53f2-bd29-4188-b49a-94aca397600b

> UUID
91d9fc0c-f18d-450c-b0f9-c804753d4305
----

== Referencias

. [[r1]] link:../../../products/rules/list/223/[REQ.223 Generar números con distribución uniforme].

. [[r2]] link:https://en.wikipedia.org/w/index.php?title=Universally_unique_identifier&oldid=457875938[Universally unique identifier, Wikipedia].

. [[r3]] link:https://es.wikipedia.org/wiki/Identificador_%C3%BAnico_global[Identificador Único Global].

. [[r4]] link:https://msdn.microsoft.com/en-us/library/system.guid.newguid.aspx[Guid.NewGuid Method].
