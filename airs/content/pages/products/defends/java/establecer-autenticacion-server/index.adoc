:slug: products/defends/java/esquema-autenticacion-server/
:category: java
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en Java al establecer un esquema de autenticación para la aplicación. Una vez configurados los roles de seguridad, el paso siguiente es definir la autenticación en el servidor.
:keywords: Java, Seguridad, Esquema, Autenticación, Servidor, Buenas Prácticas.
:defends: yes

=  Establecer Autenticación en Servidor

== Necesidad

Establecer esquema de autenticación en servidor para aplicación web +Java+

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido

. Se dispone de un servidor de aplicaciones o contenedor de +Servlets+
conforme con las especificaciones empresariales de +Java+.
. Se conoce como configurar roles de seguridad
en el descriptor de despliegue +web.xml+.
* De lo contrario, ver +Webapp Authorization URL+ en +Java+
. Se ha configurado los roles de seguridad necesarios
por la aplicación en el servidor de aplicaciones.
* Para +Tomcat+: link:../definir-rol-seguridad-tomcat/[Definir Roles de Seguridad en Tomcat]
. Se desea emplear un mecanismo genérico de control de acceso.
* Para conocer una forma alternativa
cuando se está utilizando la tecnología de +Servlets+,
ver +Access control servlets declarative+
. Se requiere un diseño personalizado para la pantalla de autenticacion.

== Solución

. Inicialmente se configuran los usuarios que tendrán acceso a la aplicación.
Este paso dependerá del contenedor de +servlets+ que se esté usando
y de la fuente de donde se extraigan los datos.
Por ejemplo en +Tomcat+,
y tomando como usuarios los definidos para el propio servidor,
se deberá agregar en el archivo +tomcatusers.xml+
usuarios con roles específicos, por ejemplo:
+
.tomcatusers.xml
[source, xml, linenums]
----
<tomcat-users>
  <role rolename="tomcat"/>
  <user username="fsg" password="Flu-1.D/" roles="tomcat"/>
</tomcat-users>
----

. A continuación se muestra la estructura de carpetas
que debe contener la aplicación:
+
[source, bash, linenums]
----
% ls -R

.:
cerrar.jsp index.jsp login-failed.jsp login.jsp WEB-INF/
./WEB-INF:
web.xml
----

. Se crea el archivo +cerrar.jsp+, página encargada de invalidar la sesión.
Para esto llama el método +invalidate+ del objeto de sesión +session+.

. La acción solo se lleva a cabo cuando se pasa como parámetro en la petición
el valor +invalidate+ en el parámetro +action+.
+
[source, html,linenums]
----
<html>
  <head>
    <title>Cierre de sesi&aocute;n</title>
  </head>
  <body>
    <%
      String action = request.getParameter("action");
      if("invalidate".equals(action)) {
        session.invalidate();
----

. Luego se obtiene una nueva sesión y se redirecciona a +index.jsp+.
+
[source, html,linenums]
----
        request.getSession(true);
        response.sendRedirect("index.jsp");
      }
    %>
  </body>
</html>
----

. Se crea el archivo +index.jsp+, el cual contendrá la página
cuyo acceso debe ser restringido.

. Únicamente posee un +link+ y hace una petición a +cerrar.jsp+
con los parámetros para cerrar la sesión.
+
[source, html,linenums]
----
Esta página puede ser vista solo por usuarios autorizados.
<br /><a href="cerrar.jsp?action=invalidate">Cerrar sesion</a>
----

. Se crea la página que contendrá el formulario de autenticación +login.jsp+
+
[source, html,linenums]
----
<html>
  <head>
    <title>Login</title>
  </head>
  <body>
----

. En el formulario, la acción corresponde a +j_security_check+
no necesita ser programada, sino que el servidor responderá ante esta
según se configure el descriptor de despliegue +web.xml+
como se verá más adelante.

. Se usa el método +response.encodeURL+
para que en caso de que las +cookies+ estén deshabilitadas,
se mantenga el +ID+ de la sesión como parte de la +URL+.
+
[source, html,linenums]
----
<form method="POST" action='<%= response.encodeURL("j_security_check") %>' >
----

. El contenido del formulario incluye los campos +j_username+ y +j_password+
que son los interpretados por el servidor para determinar el acceso.
+
[source, html,linenums]
----
       <table border="0" cellspacing="5">
        <tr><th align="right">Usuario:</th>
          <td align="left"><input type="text" name="j_username"></td>
        </tr>
        <tr><th align="right">Contrase&ntilde;a:</th>
          <td align="left"><input type="password" name="j_password"></td>
        </tr>
        <tr><td align="right"><input type="submit" value="Ingresar"></td>
          <td align="left"><input type="reset" value="Limpiar"></td>
        </tr>
      </table>
    </form>
  </body>
</html>
----

. Se crea el archivo +login-failed.jsp+ que será el mostrado
cuando se haga un intento de autenticación erróneo.
Esta página simplemente tiene un link de redirección hacia +index.jsp+.
+
[source, html,linenums]
----
<html>
 <head>
    <title>P&aacute;gina de error</title>
 </head>
 <body>
   Nombre de usuario o contrase&ntilde;a no v&aacute;lido.
   <br/><a href='<%= response.encodeURL("login.jsp") %>'>Volver</a>
 </body>
</html>
----

. Finalmente deben ser enlazadas las páginas anteriores
desde la especificación estándar de la aplicación web +WEB-INF\web.xml+
+
[source, xml, linenums]
----
<?xml version="1.0" encoding="UTF-8"?>
 <web-app id="tomcat-demo" version="2.4"
   xmlns="http://java.sun.com/xml/ns/j2ee"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee
   http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd">
 <security-constraint>
   <web-resource-collection>
   <web-resource-name>Todo</web-resource-name>
     <url-pattern>/*</url-pattern>
     <http-method>GET</http-method>
     <http-method>POST</http-method>
   </web-resource-collection>
   <auth-constraint>
     <role-name>tomcat</role-name>
    </auth-constraint>
    <user-data-constraint>
      <!-- transport-guarantee can be CONFIDENTIAL, INTEGRAL, or NONE -->
      <transport-guarantee>NONE</transport-guarantee>
    </user-data-constraint>
 </security-constraint>
----

. El tag +<security-constraint>+
se usa para definir los privilegios de acceso
a una colección de recursos usando mapeos de +URL+ ^<<r1,[1]>>^.
En este caso se está restringiendo el acceso a todas las direcciones,
sin importar si se usa el método +GET+ o +POST+,
solo a usuarios con el rol +tomcat+.
+user-data-constraint+ se dejara con el valor de +NONE+
puesto que no se requieren configuraciones adicionales para +SSL+.

. Para evitar warnings del tipo
+INFO: WARNING: Security role name tomcat used in an <auth-constraint>
without being defined in a <security-role>+,
se debe definir todos los roles usando el +tag+ +<security-role>+.
+
[source, xml,linenums]
----
<login-config>
 <auth-method>FORM</auth-method>
 <form-login-config>
   <form-login-page>/login.jsp</form-login-page>
   <form-error-page>/login-failed.jsp</form-error-page>
 </form-login-config>
</login-config>
</web-app>
----

. En +<login-config>+ se especifica cual será la página de +login+
a la que se redirigirá
cuando no se cuente con los privilegios para acceder a un recurso así
como la página de error a la que se redirigirá
cuando se intente una autenticación errónea.

== Referencias

. [[r1]] link:http://jdiezfoto.es/informatica/java-ee-seguridad-en-aplicaciones-web-i/[Java EE: Seguridad en aplicaciones web]
. [[r2]] link:https://docs.oracle.com/javaee/6/tutorial/doc/gkbaa.html[Java EE6 Tutorial - Securing Web Applications]
