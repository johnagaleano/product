:slug: products/defends/java/evitar-codigo-duplicado/
:category: java
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en Java al evitar código duplicado. El código duplicado agrega complejidad innecesaria y representa una fuente potencial de riesgos a la seguridad de la aplicación.
:keywords: Java, Seguridad, Evitar, Código, Duplicado, Buenas Prácticas.
:defends: yes

= Evitar Código Duplicado en Java

== Necesidad

Evitar código fuente duplicado en +Java+

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se está desarrollando una aplicación en +Java+.
. Se desea evitar código fuente duplicado en la aplicación.^<<r1,[1]>>,<<r2,[2]>>^

== Solución

Es común encontrar código duplicado en diferentes tipos de aplicaciones,
esta duplicación representa trabajo, riesgo y complejidad adicional,
por lo que evitar duplicar código fuente
es importante para mantener la seguridad de la aplicación ^<<r3,[3]>>^.

Esta solución presenta una manera de evitar
duplicar código fuente en aplicaciones +Java+.

. Declaramos una nueva clase y una constante para depurar los mensajes.
+
.Reuse.java
[source, java, linenums]
----
public class Reuse {
  private static final boolean DEBUG = true;
----

. El método +main+ llama a dos métodos que generan errores.
Estos errores son impresos en pantalla
si la constante de depuración tiene el valor verdadero.
+
[source, java, linenums]
----
public static void main(String[] args) {
  divisionPorCero();
  integerParseInt();
}
----

. El primer método arroja un error de división por cero.
Vemos que los mensajes de error son impresos a través de la salida +err+,
con la causa y el mensaje de excepción:
+
[source, java, linenums]
----
public static void divisionPorCero() {
  try {
    System.out.println("Resultado: " + (1337 / 0));
  }
  catch (Exception e) {
    if (DEBUG) {
      System.err.println("Error");
      System.err.println("Causa: " + e.getCause());
      System.err.println("Mensaje: " + e.getMessage());
    }
  }
}
----

. Igualmente, se define un método
que arroja la excepción de análisis de la clase +Integer+.
+
[source, java, linenums]
----
  public static void integerParseInt() {
    try {
      System.out.println("Resultado: " + Integer.parseInt("fluid"));
    }
    catch (Exception e) {
      if (DEBUG) {
        System.err.println("Error");
        System.err.println("Causa: " + e.getCause());
        System.err.println("Mensaje: " + e.getMessage());
      }
    }
  }
}
----

. Como se puede apreciar, la duplicación del código
se da en el uso de las excepciones en ambos métodos.
El problema se presenta porque al realizar posibles cambios en la aplicación
como eliminar el mensaje "Causa"
o filtrar ciertas palabras de las excepciones, entre otros,
pueden llegar a causar una fuga de información.

. En cambio, si los mensajes de error son manejados de forma centralizada,
el cambio se hace únicamente en un lugar,
por lo que un cambio futuro no representaría riesgo de fuga de información.

. Para evitar duplicar código
se debe abstraer las llamadas en común a un nuevo método.

. Declaramos la nueva clase y una constante para depurar los mensajes:
+
[source, java, linenums]
----
public class ReuseGood {
  private static final boolean DEBUG = true;
----

. El método main llama a dos métodos que generan errores.
Estos errores son impresos en pantalla
si la constante de depuración tiene el valor verdadero.
+
[source, java, linenums]
----
public static void main(String[] args) {
  divisionPorCero();
  integerParseInt();
}
----

. El método +showError+ se encargará, de ahora en adelante,
de manejar los mensajes de error.
Un cambio en este método se ve reflejado en todas sus llamadas:
+
[source, java, linenums]
----
public static void showError(Exception e) {
  if (DEBUG) {
    System.err.println("Error");
    System.err.println("Causa: " + e.getCause());
    System.err.println("Mensaje: " + e.getMessage());
  }
}
----

. Ahora los dos métodos llaman al método +showError+.
+
[source, java, linenums]
----
  public static void divisionPorCero() {
    try {
      System.out.println("Resultado: " + (1337 / 0));
    }
    catch (Exception e) {
      showError(e);
    }
  }
  public static void integerParseInt() {
    try {
      System.out.println("Resultado: " + Integer.parseInt("fluid"));
    }
    catch (Exception e) {
      showError(e);
    }
  }
}
----

== Descargas

Puedes descargar el código fuente
pulsando en los siguientes enlaces:

[button]#link:src/reuse.java[Reuse.java]#
Código que reutiliza lineas.

[button]#link:src/reusegood.java[ReuseGood.java]#
Código que no reutiliza lineas.

== Referencias

. [[r1]] link:../../../products/rules/list/162/[REQ.162 Eliminar código redundante].
. [[r2]] link:../../programacion/evitar-cod-duplicado/[Evitar Código Fuente Duplicado].
. [[r3]] link:https://es.wikipedia.org/wiki/C%C3%B3digo_duplicado[Código duplicado].
