:slug: products/defends/java/prevenir-ataque-csrf/
:category: java
:description: Nuestros ethical hackers explican como evitar vulnerabilidades de seguridad mediante la programacion segura en Java al prevenir ataques de tipo CSRF. Éstos ataques son comunes en aplicaciones que no verifican la autorización de los usuarios y son susceptibles a la suplantación de identidad.
:keywords: Java, Seguridad, Buenas Prácticas, Cross site request forgery, CSRF, JEE.
:defends: yes

= Prevenir Ataques de Cross Site Request Forgery

== Necesidad

Instalar +OWASP CSRFGuard v3+ para prevenir ataques +CSRF+ (+Java+, +JEE+)

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se está desarrollando una aplicación en +JEE+.
. Se cuenta con un contenedor de +servlets+ en funcionamiento.
. Se entiende el tipo de ataques +CSRF+.

== Solución

Las aplicaciones web deben verificar que la solicitud
enviada por un usuario sea auténtica y consistente.
De este modo se pueda determinar que la petición
ha sido intencionalmente enviada por el usuario.

Un ataque tipo +CSRF+ es aquel que fuerza al explorador web de la víctima
a enviar una petición a una aplicación web vulnerable,
entonces, esta realiza la acción seleccionada por el atacante
con el fin de suplantar a la víctima.

Estas vulnerabilidades son muy comunes en aplicaciones que no verifican
la autorización para realizar acciones vulnerables.
También ocurren cuando la aplicación procesa las credenciales dadas
como parte de la petición. Por ejemplo:

.link.shell
[source, shell, linenums]
----
http://www.sitio.com/accion/?usuario=usr&contrasena=pass
----

Insertar valores aleatorios en los formularios
es una técnica que se basa en la  generación y codificación
de un número aleatorio (+token+).
Este +token+ es almacenado en la sesión del usuario
luego de haberse autenticado.
Este token se debe generar en cada formulario
y debe incluirse en un campo oculto dentro del formulario.

A la recepción de los campos del formulario en el servidor,
se comprueba que el +token+ recibido
coincida con el almacenado para el usuario.
Si el +token+ no coincide se aborta la acción del formulario ^<<r1,[1]>>^.

En +Java EE+ se puede implementar este mecanismo
haciendo uso de +CSRFGuard+ ^<<r2,[2]>>^.
Para instalarlo se deben considerar los siguientes pasos:

. Descargar +OWASP CSRFGuard 3+ o superior ^<<r2,[2]>>^.

. Copiar los archivos +OWASP CSRFGuard+ en el directorio
de la aplicación de la siguiente forma:
+
[source, shell, linenums]
----
${PROJECT}/WEB-INF/lib/Owasp.CsrfGuard.jar
${PROJECT}/WEB-INF/Owasp.CsrfGuard.properties
${PROJECT}/WEB-INF/Owasp.CsrfGuard.js
----

. Modificar el archivo +Owasp.CsrfGuard.properties+
para indicar que archivos no requieren ser protegidos.
El identificador entre +org.owasp.csrfguard.unprotected+
y el +=+ puede ser un nombre cualquiera.
Después del +=+ se debe especificar un patrón para especificar las paginas.
Algunos ejemplos:
+
[source, shell, linenums]
----
org.owasp.csrfguard.unprotected.Default=/ProgSeg/
org.owasp.csrfguard.unprotected.Index=/ProgSeg/index.jsp
org.owasp.csrfguard.unprotected.MiServlet=/ProgSeg/MiServlet
org.owasp.csrfguard.unprotected.Html=*.html
org.owasp.csrfguard.unprotected.Public=/ProgSeg/Public/*
----

. También se debe configurar
el descriptor de despliegue +web.xml+,
ubicado en +${PROJECT}/WEB-INF/web.xml+.

. Primero se configuran los parámetros del contexto
indicando la ruta del archivo +Owasp.CsrfGuard.properties+
que contiene las propiedades de configuración de la biblioteca.
+
[source, xml,linenums]
----
<web-app reloadable="true" version="3.0"
 xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
 http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd">
 <context-param>
   <param-name>Owasp.CsrfGuard.Config</param-name>
   <param-value>WEB-INF/Owasp.CsrfGuard.properties</param-value>
 </context-param>
----

. Luego se declara y se habilita el +HttpSessionListener+ de +CSRFGuard+.
Esto se hace usando como nombre de la clase
+org.owasp.csrfguard.CsrfGuardListener+.
Esta clase es responsable de consumir los parámetros del contexto
e inicializar el contexto de +CsrfGuard+
para todas las nuevas +HttpSessions+.
+
[source, xml,linenums]
----
<listener>
 <listener-class>org.owasp.csrfguard.CsrfGuardListener</listener-class>
</listener>
----

. Se debe agregar un filtro en el archivo descriptor +web.xml+.
Los filtros de +Java EE+ proporcionan la capacidad para interceptar,
ver y modificar tanto la solicitud como la respuesta asociada
con la petición del cliente.
Para que la aplicación haga uso de +OWASP CSRFGuard+,
se debe declarar el filtro y los parámetros de configuración.
+
[source, xml,linenums]
----
<filter>
  <filter-name>CSRFGuard</filter-name>
  <filter-class>org.owasp.csrfguard.CsrfGuardFilter</filter-class>
</filter>
----

. Se debe agregar un mapeo para el nuevo filtro ingresado.
Después de haber declarado el filtro en el descriptor,
se debe especificar a que recurso el filtro estará asociado.
Se puede configurar el filtro para que manipule todas las solicitudes
que reciba un +servlet+ específico o un patrón +URL+.

. Configurar adecuadamente los +request+:
+
[source, xml, linenums]
----
<filter-mapping>
  <filter-name>CSRFGuard</filter-name>
  <url-pattern>/*</url-pattern>
</filter-mapping>
</web-app>
----


La declaración anterior indica que manipule
todos los +request+ direccionados a cualquier página.

== Referencias

. [[r1]] link:https://cwe.mitre.org/data/definitions/352.html[CWE-352: Cross-Site Request Forgery (CSRF)].
. [[r2]] link:https://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project[OWASP CSRFGuard Project].
. [[r3]] link:../../../products/rules/list/174/[REQ.174 Transacciones sin patrón discernible]
