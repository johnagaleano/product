:slug: products/defends/java/codificar-base64/
:category: java
:description: Nuestros ethical hackers explican que es Base64 y como funciona su algoritmo de cifrado, además enseñan la manera de cómo codificar y decodificar en base64 una aplicación desplegada en JBoss usando la biblioteca org.apache.commons.codec.binary.Base64.
:keywords: Java, Cifrar, Base64, JBoss, Ascii, Descifrar.
:defends: yes

= Codificar en Base64

== Necesidad

Codificar en +base64+ una aplicación +Java+ ejecutándose sobre +JBoss+.

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se tiene una aplicación desplegada sobre +JBoss EAP 4.3+.
. Se tiene instalada la versión +1.6+ de +Java+ (+JRE+).

== Solución

+Base64+ es un grupo de esquemas de codificación de binario a texto,
es decir, es un tipo de codificación de transporte de datos
que tiene la finalidad de proteger la información
que se envía entre dos o más ordenadores.
Esta codificación se hace representando los datos binarios
mediante una cadena +ASCII+ traduciéndolos en una representación radix-64.
La finalidad de usar este tipo de codificación,
es para asegurar que los datos se mantienen intactos
y sin modificaciones durante la transmisión.
Además, +Base64+ es comúnmente usado en muchas aplicaciones,
incluyendo la escritura de +emails+ vía +MIME+
y el almacenamiento de datos complejos en +XML+ ^<<r1,[1]>>^.

La codificación en +base64+ es relativamente sencilla;
primero, se convierten los valores +ASCII+ de los carácteres
que forman el mensaje que se quiere cifrar a binario,
quedando +1 byte+ por cada carácter.
Segundo, juntan todos los +bytes+ y se separan en grupos de 6 bits.
Por último, se convierten esos grupos de +6 bits+ a letras
según la tabla de carácteres base64 ^<<r2,[2]>>^.
En caso de que quede un grupo de menos de +6 bits+,
se agrega el carácter +=+ hasta completarlos.

. +Java+ provee de modo nativo
una clase llamada +javax.xml.bind.DatatypeConverter+
que provee los métodos +printBase64Binary+
y +parseBase64Binary+ que sirven para codificar
y decodificar +Base64+ respectivamente.
Sin embargo, esta clase
arroja la excepción +NullPointerException+
cuando es utilizada desde el servidor de aplicación +JBoss+.

. Para este caso, se recomienda usar una biblioteca de terceros
que provee la misma funcionalidad.
Esa biblioteca es +org.apache.commons.codec.binary.Base64+
y usa los métodos +encodeBase64String+
y +decodeBase64+ que son reemplazo directo
para los métodos previamente mencionados.

. A continuación un código de ejemplo para mostrar su uso.
+
.TestBase64.java
[source, java, linenums]
----
/* Este es un código de prueba, por tanto no debe ser desplegado en producción.
Para mantener el código simple, se imprime por salida estándar, sin embargo
en el código real debe utilizarse el mecanismo estándar de biácoras (logs).
*/

import org.apache.commons.codec.binary.Base64;
import java.nio.charset.Charset;
import java.util.Arrays;

public class TestBase64 {
 public static void main (String... args) throws java.io.UnsupportedEncodingException {

  // Se define texto plano y codificado originales

 String originalExamplePlain = "Texto de ejemplo incluyendo caracteres como
    A-Z, a-z, 0-9, = y /.";
 String originalExampleBase64 =
  "VGV4dG8gZGUgZWplbXBsbyBpbmNsdXllbmRvIGNhcmFjdGVyZXMgY29tbyBBLVosIGEteiwgMC05LCA9IHkgLy4=";

 /* Se obtiene la representacion en bytes de los datos originales asumiendo que su
     codificación es ASCII*/

 byte[] bytesExamplePlain = originalExamplePlain.getBytes("US-ASCII");
 byte[] bytesExampleBase64 = originalExampleBase64.getBytes("US-ASCII");

 // Se comprueba que en realidad los datos originales sí estuvieran en ASCII

 String examplePlain = new String(bytesExamplePlain, Charset.forName("USASCII"));
 String exampleBase64 = new String(bytesExampleBase64, Charset.forName("USASCII"));
 assert examplePlain.equals(originalExamplePlain);
 assert exampleBase64.equals(originalExampleBase64);

 // Se obtiene la conversión a texto plano de la representación base64 original

 byte[] convertedToPlain = Base64.decodeBase64(exampleBase64);

 /* Se comprueba que la conversión corresponda a los bytes originales del
    texto plano*/

 assert Arrays.equals(convertedToPlain, bytesExamplePlain);

 // Se obtiene la conversión a base64 del texto plano original

 String convertedToBase64 = Base64.encodeBase64String(bytesExamplePlain);

 // Se comprueba que la conversión corresponda al texto plano original

 assert convertedToBase64.equals(exampleBase64);

 // Se muestra por salida estandar el resultado de la conversion

 System.out.format("%s\n", new String(convertedToPlain, "US-ASCII"));
 System.out.format("%s\n", convertedToBase64);
 }
}
----

. Para ejecutarlo, se requiere descargar
la biblioteca +Apache Commons Codec+ ^<<r3,[3]>>^.

. La clase se compila del siguiente modo.
+
[source, bash, linenums]
----
% javac -cp commons-codec.jar TestBase64.java
----

. Para ejecutarla, se incluirá el argumento +-ea+
para que se validen las aserciones (+asserts+).
+
[source, bash, linenums]
----
java -cp commons-codec.jar:. -ea TestBase64

Texto de ejemplo incluyendo caracteres como A-Z, a-z, 0-9, = y /.
VGV4dG8gZGUgZWplbXBsbyBpbmNsdXllbmRvIGNhcmFjdGVyZXMgY29tbyBBLVosIGEteiwgMC05LCA9IHk
gLy4=
----

== Descargas

Puedes descargar el código fuente
pulsando en el siguiente enlace:

[button]#link:src/testbase64.java[TestBase64.java]#
Clase TestBase64.

== Referencias

. [[r1]] link:https://developer.mozilla.org/es/docs/Web/API/WindowBase64/Base64_codificando_y_decodificando[Base64 codificando y decodificando]
. [[r2]] link:https://en.wikipedia.org/wiki/Base64[Base64]
. [[r3]] link:http://commons.apache.org/proper/commons-codec/[Apache Commons Codec]
