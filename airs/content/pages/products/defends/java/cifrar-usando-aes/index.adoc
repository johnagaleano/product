:slug: products/defends/java/cifrar-usando-aes/
:category: java
:description: Nuestros ethical hackers explican en que consiste y como funciona el algoritmo de cifrado AES (uno de los cifrados más seguros hasta el momento). Además, mediante un ejemplo desarrollado en Java enseñan como utilizarlo para cifrar y descifrar información usando.
:keywords: Java, Seguridad, AES, Cifrado, Criptografía, Algoritmo.
:defends: yes

= Cifrar Información Usando el Algoritmo AES

== Necesidad

Cifrar información mediante el algoritmo de cifrado simétrico +AES+.

== Contexto

A continuación se describen las circunstancias
bajo las cuales la siguiente solución tiene sentido:

. Se dispone de un compilador estándar de +Java+.

== Solución

El algoritmo +Advanced Encryption Standard+ (+AES+)
es un algoritmo de cifrado simétrico e interactivo que,
valga la redundancia, cifra la información
haciendo uso de varias sustituciones,
permutaciones y transformaciones lineales,
ejecutadas en bloques de datos de +16 bytes+
que se repiten una y otra vez.

Normalmente, el método de cifrado de +AES+
se podría describir de la siguiente manera:
+AES+ crea y almacena una clave de cifrado principal
en una matriz con el fin de generar
una tabla que contiene varias claves.
Estas claves se conocen como claves redondas.

Además, también divide la información a cifrar
en bloques de +16 bits+ los cuales son copiados
en una matriz bidimensional llamada matriz de estado.

Una vez tiene la matriz de estado y las claves redondas,
realiza una operación +XOR+ entre ambas.
Después, realiza una sustitución de +bytes+
sirviéndose de una tabla de sustitución adicional.
Seguidamente, realiza un cambio
entre las mismas filas de la matriz de estado.
Por último, realiza el cambio
entre las columnas de la matriz.
Este proceso se repite por lo menos 10 veces.

Una vez que se completa la ejecución,
el algoritmo muestra la matriz de estado
en forma de texto cifrado.

Una vez entendido el funcionamiento del algoritmo +AES+
miraremos la manera de cifrar información
usando el algoritmo en un programa desarrollado en +Java+.

. Java proporciona la clase +Cipher+
la cual pertenece al paquete +javax.crypto+
y ofrece las funcionalidades
de un algoritmo de cifrado criptográfico
para cifrar y descifrar información.

. Entonces, para utilizar el algoritmo de cifrado simétrico +AES+
se debe importar la clase +javax.crypto.Cipher+.
Además, también se deben importar las clases
+javax.crypto.KeyGenerator+ y +javax.crypto.SecretKey+
que serán utilizadas para generar la clave secreta
con la cual se va a cifrar y descifrar la información.
+
.cli.java
[source, java, linenums]
----
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
----

. De igual forma, hay que importar algunas clases
que permitirán tratar las excepciones
que ocurran durante el proceso.
Estas excepciones deben ser controladas
por las clases que consuman
el servicio de cifrado simétrico.
+
[source, java, linenums]
----
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
----

. Para utilizar una instancia del algoritmo +AES+
se debe utilizar el método estático
+getInstance+ de la clase +Cipher+
especificando el nombre del algoritmo.
+
[source, java, linenums]
----
class CLI {
  public static void main(String args[])
    throws NoSuchAlgorithmException, NoSuchPaddingException,
      BadPaddingException, InvalidKeyException, IllegalBlockSizeException {
        String plainText = "This is just an example";
        String algorithm = "AES";
        Cipher cipher = Cipher.getInstance(algorithm);
----

. Para generar la clave secreta
con la cual se va a cifrar y descifrar la información,
se debe invocar el método estático
+getInstance+ de la clase +KeyGenerator+
con el fin de obtener una referencia al objeto
que implementa el servicio de generación de clave +AES+.
Posteriormente, se establece el tamaño y se genera la clave.
Por defecto, el tamaño es de +128 bits+.
+
[source, java, linenums]
----
KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm);
keyGenerator.init(256);
SecretKey secretKey = keyGenerator.generateKey();
----

. Para cifrar la información
se debe establecer el modo de operación +ENCRYPT_MODE+
y pasar como parámetro la clave secreta
con la cual se va a cifrar la información.
+
[source, java, linenums]
----
cipher.init(Cipher.ENCRYPT_MODE, secretKey);
byte[] encrypText = cipher.doFinal(plainText.getBytes());
----

. Para descifrar la información
se debe establecer el modo de operación +DECRYPT_MODE+
y pasar como parámetro la misma clave secreta
que se utilizó para cifrar el contenido.
+
[source, java, linenums]
----
cipher.init(Cipher.DECRYPT_MODE, secretKey);
byte[] decrypText = cipher.doFinal(encrypText);
----

. Finalmente se imprimen los resultados obtenidos
tanto cuando se cifra como cuando se descifra el texto.
+
[source, java, linenums]
----
  String encodedEncText =
    javax.xml.bind.DatatypeConverter.printBase64Binary(encrypText);
  System.out.println("Encrypted text: " + new String(encrypText));
  System.out.println("Encrypted and encoded text: " + encodedEncText);
  System.out.println("Decrypted text: " + new String(decrypText));
  }
}
----

. Se compila y ejecuta el programa
+
.compile.java
[source, java, linenums]
----
$ javac CLI.java
$ java CLI
Encrypted text:
~5=GU&#65533;&#65533;V&#65533;`&#65533;&#1432;&#493;C&#65533;N&#65533;fcy&#65533;@J&#65533;&
#65533;&#1166;
Encrypted and encoded text: fjU9R1Xr/FbAYOXWmMetEUOZFk7vZmN55BpASq7H0o4=
Decrypted text: This is just an example
----

== Descargas

Puedes descargar el código fuente
pulsando en el siguiente enlace:

[button]#link:src/cli.java[CLI.java]#
Clase CLI.

== Referencias

. [[r1]] link:https://csrc.nist.gov/projects/cryptographic-standards-and-guidelines/archived-crypto-projects/aes-development[AES Development]
. [[r2]] link:https://www.veracode.com/blog/research/encryption-and-decryption-java-cryptography[Encryption and Decryption in Java Cryptography]
