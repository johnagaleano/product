# Standard libraries
import os
from typing import List
import re

# Third party libraries

# Local libraries
from toolbox.drills import generic as drills_generic
from toolbox.utils.function import shield
from toolbox import (
    logger,
    utils,
)
from toolbox.utils.integrates import (
    get_filter_rules,
)


def translate_glob_pattern(pattern: str) -> str:
    # Escape everything that is not `*` and replace `*` with regex `.*`
    expression = r'.*'.join(map(re.escape, pattern.split('*')))
    return f'^{expression}$'


def notify_out_of_scope(
    repo_name: str,
    include_regexps: str,
    exclude_regexps: str,
) -> bool:
    logger.info(f'Please remember the scope for : {repo_name}')
    logger.info('  In scope:')
    for regex in include_regexps:
        logger.info(f'    - {regex}')

    if exclude_regexps:
        logger.info()
        logger.info('  Out of scope:')
        for regex in exclude_regexps:
            logger.info(f'    - {regex}')

    logger.info()

    return True


def delete_out_of_scope_files(group: str) -> bool:
    # This entire function should be rewritten:
    #   https://gitlab.com/fluidattacks/product/-/issues/2617#note_474753627
    # I'm patching it for now (2020-12-28) so it survives a few days
    # The business logic is going to change when we all get to an agreement

    path_to_fusion: str = os.path.join('groups', group, 'fusion')

    for root in get_filter_rules(group):
        # Please modify this piece of code, it's too dirty to assume
        # that the url contains the name of the repo:
        # - it can end in .git,
        # - it can contain query parameters
        # - it can be url encoded
        # Besides, testing for `a in b` may cause false positives:
        # For instance:
        #   url = github/group/repo, is the github url for repo
        #   if you are looking for: folder = group
        # then you consider the url is a match, but it is not
        # This is a critical component, treat it as such
        # 100% determinism

        repos = [
            folder for folder in os.listdir(path_to_fusion)
            if folder in root['url'].split('/')[-1]
        ]
        if repos:
            repo_name = repos[0]
        else:
            logger.warn(f'can not find a repository for {root["url"]}')
            continue

        # Compute what files should be deleted according to the scope rules
        non_matching_files_iterator = utils.file.iter_non_matching_files(
            path=path_to_fusion,
            include_regexps=tuple(map(
                translate_glob_pattern,
                root['filter']['include'],
            )),
            exclude_regexps=tuple(map(
                translate_glob_pattern,
                root['filter']['exclude'],
            )),
        )

        # Display to the user the Scope
        notify_out_of_scope(
            repo_name,
            root['filter']['include'],
            root['filter']['exclude'],
        )

        for path in non_matching_files_iterator:
            if path.startswith(repo_name) and '.git' not in path:
                path = os.path.join(path_to_fusion, path)
                if os.path.isfile(path):
                    os.unlink(path)
                elif os.path.isdir(path):
                    os.removedirs(path)

    return True


def pull_repos_s3_to_fusion(subs: str,
                            local_path: str,
                            repository_name: str = 'all') -> bool:
    '''
    Download repos from s3 to a provided path

    param: subs: group to work with
    param: local_path: Path to store downloads
    '''

    if repository_name != 'all':
        local_path = f'{local_path}/{repository_name}'
    else:
        repository_name = ''

    bucket_path: str = f's3://continuous-repositories/' \
        f'{subs}/{repository_name}'

    os.makedirs(local_path, exist_ok=True)

    aws_sync_command: List[str] = [
        'aws', 's3', 'sync',
        '--delete',
        '--sse', 'AES256',
        '--exact-timestamps',
        bucket_path, local_path,
    ]

    git_expand_repositories_command: List[str] = [
        'find', local_path,
        '-name', '.git',
        '-execdir',
        'git', 'checkout',
        '--', '.', ';'
    ]
    logger.info(f'Downloading {subs} repositories')

    # Passing None to stdout and stderr shows the s3 progress
    # We want the CI to be as quiet as possible to have clean logs
    kwargs = dict() if utils.generic.is_env_ci() else dict(
        stdout=None,
        stderr=None,
    )

    status, stdout, stderr = utils.generic.run_command(
        cmd=aws_sync_command,
        cwd='.',
        env={},
        **kwargs,
    )

    if status:
        logger.error('Sync from bucket has failed:')
        logger.info(stdout)
        logger.info(stderr)
        logger.info()
        return False

    git_status, git_stdout, git_stderr = utils.generic.run_command(
        cmd=git_expand_repositories_command,
        cwd='.',
        env={},
        **kwargs,
    )

    if git_status:
        logger.error('Expand repositories has failed:')
        logger.info(git_stdout)
        logger.info(git_stderr)
        logger.info()
        return False
    return True


@shield(retries=1)
def main(subs: str, repository_name: str = 'all') -> bool:
    '''
    Clone all repos for a group

    param: subs: group to work with
    '''
    bucket: str = 'continuous-repositories'
    passed: bool = True
    if not utils.generic.does_subs_exist(subs):
        logger.error(f'group {subs} does not exist.')
        passed = False
        return passed

    utils.generic.aws_login(f'continuous-{subs}')

    if not drills_generic.s3_path_exists(bucket, f'{subs}/'):
        logger.error(f'group {subs} does not have repos uploaded to s3')
        passed = False
    else:
        local_path: str = f'groups/{subs}/fusion/'

        logger.info('Computing last upload date')
        days: int = \
            drills_generic.calculate_days_ago(
                drills_generic.get_last_upload(bucket, f'{subs}/'))

        passed = passed \
            and pull_repos_s3_to_fusion(subs, local_path, repository_name) \
            and delete_out_of_scope_files(subs)

        logger.info(f'Data for {subs} was uploaded to S3 {days} days ago')

    return passed
