import { StyleSheet } from "react-native";

export const styles: Record<
  string,
  StyleSheet.NamedStyles<{}>
> = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
  },
  text: {
    color: "#808080",
  },
});
