# Standard libraries
from typing import NamedTuple


class Credentials(NamedTuple):
    user: str
    key: str
