export interface IHandleAcceptationButtonProps {
  isEditing: boolean;
  isRequestingReattack: boolean;
  isVerifying: boolean;
  openHandleAcceptation: () => void;
}
