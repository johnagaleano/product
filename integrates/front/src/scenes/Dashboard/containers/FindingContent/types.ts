export interface IHeaderQueryResult {
  finding: {
    analyst?: string;
    closedVulns: number;
    exploit: string;
    historicState: Array<{
      analyst: string; date: string; state: string;
    }>;
    id: string;
    openVulns: number;
    releaseDate: string;
    severityScore: number;
    state: "open" | "closed" | "default";
    title: string;
  };
}
