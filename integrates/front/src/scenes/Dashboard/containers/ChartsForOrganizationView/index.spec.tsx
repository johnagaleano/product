import { ChartsForOrganizationView } from "scenes/Dashboard/containers/ChartsForOrganizationView";

describe("ChartsForOrganizationView", () => {

  it("should return an function", () => {
    expect(typeof (ChartsForOrganizationView))
      .toEqual("function");
  });

});
