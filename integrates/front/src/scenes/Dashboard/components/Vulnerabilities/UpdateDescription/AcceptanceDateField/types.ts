import type { IHistoricTreatment } from "scenes/Dashboard/containers/DescriptionView/types";

interface IAcceptanceDateFieldProps {
  isAcceptedSelected: boolean;
  lastTreatment: IHistoricTreatment;
}

export { IAcceptanceDateFieldProps };
