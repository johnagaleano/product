import type { IHistoricTreatment } from "scenes/Dashboard/containers/DescriptionView/types";

interface IJustificationFieldProps {
  isTreatmentPristine: boolean;
  lastTreatment: IHistoricTreatment;
}

export { IJustificationFieldProps };
