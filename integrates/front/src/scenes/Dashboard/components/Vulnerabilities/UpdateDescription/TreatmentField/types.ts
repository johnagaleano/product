import type { IHistoricTreatment } from "scenes/Dashboard/containers/DescriptionView/types";

interface ITreatmentFieldProps {
  isTreatmentPristine: boolean;
  lastTreatment: IHistoricTreatment;
}

export { ITreatmentFieldProps };
