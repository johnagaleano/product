interface ITagFieldProps {
  handleDeletion: (tag: string) => void;
  isAcceptedSelected: boolean;
  isAcceptedUndefinedSelected: boolean;
  isInProgressSelected: boolean;
}

export { ITagFieldProps };
