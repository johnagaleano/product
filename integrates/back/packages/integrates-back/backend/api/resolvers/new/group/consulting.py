# Standard
from typing import cast, Dict, List

# Third party
from graphql.type.definition import GraphQLResolveInfo

# Local
from backend import util
from backend.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    get_entity_cache_async,
    require_integrates
)
from backend.domain import project as project_domain
from backend.typing import Comment, Project as Group


@concurrent_decorators(
    enforce_group_level_auth_async,
    require_integrates
)
@get_entity_cache_async
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None
) -> List[Comment]:
    group_name: str = cast(str, parent['name'])
    user_data: Dict[str, str] = await util.get_jwt_content(info.context)
    user_email: str = user_data['user_email']

    return await project_domain.list_comments(group_name, user_email)
