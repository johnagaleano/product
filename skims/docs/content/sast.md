
# [Static Analysis](https://en.wikipedia.org/wiki/Static_application_security_testing)

Static Application Security Testing  or _SAST_ is used to secure software by reviewing the source code in order to identify vulnerabilities.

Below are the vulnerability types and their description that Skims report:
