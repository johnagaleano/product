
## [F060][F060] - [CWE-397][CWE-397]

### Java

Code examples:

```java
public class VulnerableExample {
  // Exception is generic
  public static void example() throws Exception {};
}

public class SafeExample {
  // CustomException is not generic
  public static void example() throws CustomException {};
}
```

The following exceptions are considered generic:

- Exception
- Throwable
- lang.Exception
- lang.Throwable
- java.lang.Exception
- java.lang.Throwable

---

[CWE-397]: https://cwe.mitre.org/data/definitions/397.html
[F060]: https://fluidattacks.com/products/rules/findings/060
