
## About Skims

Skims is a vulnerability scanner tool.
This means it scans your source-code and applications and shows you the security
problems they have.

Skims is able to constantly monitor the security state of your system.
It opens new security findings as they are introduced,
and closes security findings once they are no longer present in the system.

At all moments you can read awesome reports and analytics at Integrates:

- Description of the vulnerability

  ![docs_integrates_description](https://gitlab.com/fluidattacks/product/-/raw/master/skims/static/img/docs_integrates_description.png)

- Evidence that the vulnerability exists

  ![docs_integrates_evidences](https://gitlab.com/fluidattacks/product/-/raw/master/skims/static/img/docs_integrates_evidences.png)

- Aggregated analytics

  ![docs_integrates_analytics](https://gitlab.com/fluidattacks/product/-/raw/master/skims/static/img/docs_integrates_analytics.png)

- And many more features!
