// $ javac test/data/lib_path/f060/Test.java && java test/data/lib_path/f060/Test.java

public class Test {
  public static void main(String[] args) throws Exception {};
  public static void method() throws Exception {};
  public static void multiple() throws ServletException, IOException {};
  public static void multiple_plus_attr_access1() throws ServletException, java.lang.Throwable {};
  public static void multiple_plus_attr_access2() throws ServletException, Throwable, IOException {};
  public static void multiple_plus_attr_access3() throws IOException, ServletException, CustomException {};
  public static void multiple_plus_attr_access4() throws
    IOException, ServletException, CustomException, Throwable {};
  public static void attr_access() throws java.lang.Throwable {};
}
